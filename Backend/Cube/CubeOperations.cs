﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows;

namespace rubikscube
{
    internal static class CubeOperations
    {
        /// <summary>
        /// Saves a cube to a file, location specified by the user.
        /// </summary>
        /// <param name="thisCube">The cube to save.</param>
        public static void SaveCube(Cube thisCube)
        {
            SaveFileDialog sfd = new();
            sfd.Filter = "JSON files (*.json)|*.json";
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == false)
                return;

            string path = sfd.FileName;

            try
            {
                string json = JsonConvert.SerializeObject(thisCube);
                File.WriteAllText(path, json);

                MessageBox.Show($"Cube was succesfully saved at {path}!", "Woohoo!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch
            {
                MessageBox.Show("Something went wrong while trying to save the cube.", "Oh no!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        /// <summary>
        /// Reads a cube from a file.
        /// </summary>
        /// <returns>The cube.</returns>
        public static Cube? LoadCube()
        {
            OpenFileDialog ofd = new();
            ofd.Filter = "JSON files (*.json)|*.json";
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == false)
                return null;

            Cube? cube = null;

            try
            {
                string json = File.ReadAllText(ofd.FileName);
                cube = JsonConvert.DeserializeObject<Cube>(json) ?? throw new NullReferenceException();
                cube.RedoStack.Clear();
            }
            catch
            {
                MessageBox.Show("Something went wrong while trying to open the cube.", "Oh no!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return cube;
        }

        /// <summary>
        /// Helper Method to FormatPerms() and OptimizePermsListCompounds(). Compounds consecutive duplicate perms to one string as one element.
        /// </summary>
        /// <param name="perms"></param>
        /// <returns>The compounded permutation list</returns>
        public static List<string> CompoundPermsList(List<string> perms)
        {
            List<string> processedPermsList = new List<string>();
            string consecutivePerms = "";

            void updateProcessedPermsList()
            {
                if (processedPermsList.Count > 0 && consecutivePerms[0] == processedPermsList[^1][0])
                {
                    processedPermsList[^1] += consecutivePerms;

                    if (processedPermsList[^1].Length % 4 == 0)
                        processedPermsList.RemoveAt(processedPermsList.Count - 1);
                }
                else if (consecutivePerms.Length % 4 != 0)
                    processedPermsList.Add(consecutivePerms);
            }

            for (int i = 0; i < perms.Count; i++)
            {
                string permText = perms[i];

                if (consecutivePerms == "" || permText[0] == consecutivePerms[0])
                    consecutivePerms += permText;
                else
                {
                    updateProcessedPermsList();
                    consecutivePerms = permText;
                }

                if (i == perms.Count - 1)
                    updateProcessedPermsList();
            }

            return processedPermsList;
        }

        /// <summary>
        /// Formats permutations to official rubiks cube notation.
        /// </summary>
        /// <param name="perms">Permutations to format.</param>
        /// <returns>A list of formatted permutations.</returns>
        public static List<string> FormatPerms(List<string> perms)
        {
            List<string> permsTextListFormatted = new List<string>();
            List<string> compoundedPermsList = CompoundPermsList(perms);

            for (int i = 0; i < compoundedPermsList.Count; i++)
            {
                string? suffix = null;
                switch (compoundedPermsList[i].Length % 4)
                {
                    case 1:
                        suffix = "";
                        break;
                    case 2:
                        suffix = "2";
                        break;
                    case 3:
                        suffix = "\'";
                        break;
                }

                if (suffix != null)
                    permsTextListFormatted.Add(compoundedPermsList[i][0] + suffix);
            }

            return permsTextListFormatted;
        }

        /// <summary>
        /// Optimizes a list of permutations by taking out the permutations that do nothing.
        /// </summary>
        /// <param name="perms">Permutations to format.</param>
        /// <returns>An optimized permslist.</returns>
        public static List<string> OptimizePermsList(List<string> perms)
        {
            List<string> optimizedPermsList = new List<string>();
            List<string> compoundedPermsList = CompoundPermsList(perms);

            foreach (string perm in compoundedPermsList)
            {
                switch (perm.Length % 4)
                {
                    case 0:
                        break;
                    case 1:
                        optimizedPermsList.Add(new string(perm[0], 1));
                        break;
                    case 2:
                        optimizedPermsList.Add(new string(perm[0], 1));
                        optimizedPermsList.Add(new string(perm[0], 1));
                        break;
                    case 3:
                        optimizedPermsList.Add(new string(perm[0], 3));
                        break;

                }
            }

            while (optimizedPermsList.Count > 0 && optimizedPermsList[^1][0] is 'x' or 'y' or 'z')
            {
                optimizedPermsList.RemoveAt(optimizedPermsList.Count - 1);
            }

            return optimizedPermsList;
        }
        /// <summary>
        /// Checks if a cube is in a solved state or not.
        /// </summary>
        /// <param name="c">A cube.</param>
        /// <returns>A bool which is true if the cube is in a solved state.</returns>
        public static bool CheckIfSolved(Cube c)
        {
            foreach (KeyValuePair<string, int[]> colors in c.GetGraphicsDict())
            {
                if (!colors.Value.Skip(1).All(i => int.Equals(colors.Value[0], i)))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a new scrambled cube.
        /// </summary>
        /// <param name="input">A seed the scramble is based on.</param>
        /// <returns>A scrambled cube.</returns>
        public static Cube getScrambledCube(string input)
        {
            //read value from seed input box, then scramble cube based on that
            string permsString = Backend.GeneratePermsFromMd5(Backend.GenerateMd5(input));
            List<char> permsList = permsString.ToList();

            Cube cube = new Cube(); //reset the cube to completely solved

            foreach (char c in permsList)
            {
                cube.Values = cube.PermCube(cube.Values, c, 1, updatePermsTextList: false);
            }

            return cube;
        }
    }
}
