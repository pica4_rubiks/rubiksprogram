using System;
using System.Collections.Generic;
using System.Linq;

namespace rubikscube
{
    public class Cube
    {
        public int[,,] Values; //values are the values of the cube itself in the class Cube
         
        private List<string> _permsTextList = new();
        private List<string> _solution = new();
        private bool solutionOptimal;

        /// <summary>
        /// All perms done to the cube, also function as undo stack.
        /// </summary>
        public List<string> PermsTextList
        {
            get
            { 
                return _permsTextList;
            }
            set
            {
                _permsTextList = value;
            }
        }

        public List<string> Solution
        {
            get
            {
                return _solution;
            }
            set
            {
                solutionOptimal = true;
                _solution = value;
            }
        }

        public bool SolutionKnown
        {
            get
            {
                if (!CubeOperations.CheckIfSolved(this))
                    return _solution.Count > 0;
                else return true;
            }
        }

        public bool SolutionOptimal
        {
            get
            {
                if (SolutionKnown)
                    return solutionOptimal;
                else
                    throw new Exception();
            }

            set
            {
                solutionOptimal = value;
            }
        }

        public List<string> RedoStack = new List<string>();

        public Cube(int[,,]? values = null)
        {
            Values = values ?? DefaultCube(); //when creating an instance of Cube, the default values should be the ones as stated in DefaultCube()
        }

        /// <summary>
        /// Returns a cube in solved state, in 5x5x5 array
        /// </summary>
        /// <returns> cube in solved state, in 5x5x5 array</returns>
        public static int[,,] DefaultCube()
        {
            int[,,] thedefault =
            {
                //0 = empty. 1-6 = color face. 7-33=cubie
                /* The array is 5*5 and not 3*3 as  next to every cube there is a place for the color of the adjacent face. This makes the first and last layer completely colors only
                 * 0 = leeg
                 *  1 = white
                 *  2 = blue
                 *  3 = green
                 *  4 = yellow
                 *  5 = orange
                 *  6 = red
                 */
                {
                    // slice 1 (color layer front)
                    { 0, 0, 0, 0, 0 },
                    { 0, 6, 6, 6, 0 },
                    { 0, 6, 6, 6, 0 },
                    { 0, 6, 6, 6, 0 },
                    { 0, 0, 0, 0, 0 }
                },
                {
                    // slice 2 (first layer of cubies)
                    { 0, 1, 1, 1, 0 },
                    { 3, 7, 8, 9, 2 },
                    { 3, 10, 11, 12, 2 },
                    { 3, 13, 14, 15, 2 },
                    { 0, 4, 4, 4, 0 }
                },
                {
                    // slice 3 (second layer of cubies)
                    { 0, 1, 1, 1, 0 },
                    { 3, 16, 17, 18, 2 },
                    { 3, 19, 20, 21, 2 },
                    { 3, 22, 23, 24, 2 },
                    { 0, 4, 4, 4, 0 }
                },
                {
                    // slice 4 (third layer of cubies)
                    { 0, 1, 1, 1, 0 },
                    { 3, 25, 26, 27, 2 },
                    { 3, 28, 29, 30, 2 },
                    { 3, 31, 32, 33, 2 },
                    { 0, 4, 4, 4, 0 }
                },
                {
                    // slice 5 (color layer back)
                    { 0, 0, 0, 0, 0 },
                    { 0, 5, 5, 5, 0 },
                    { 0, 5, 5, 5, 0 },
                    { 0, 5, 5, 5, 0 },
                    { 0, 0, 0, 0, 0 }
                }
            };
            return thedefault;
        }
        
        
        public Dictionary<string, int[]>
            GetGraphicsDict() //a parse function, which parses the 3D-array to the dictionary type as requested by the CubeGraphics class.
        {
            List<int>
                ints = new List<int>(); //create a list in which the color value ints of the cube will be stored, unsorted
            for (int a = 0; a < 5; a++) //add the colors
            {
                for (int b = 0; b < 5; b++)
                {
                    for (int c = 0; c < 5; c++)
                    {
                        if (Values[a, b, c] != 0 && Values[a, b, c] < 7)
                        {
                            ints.Add(Values[a, b, c]);
                        }
                    }
                }
            }

            Dictionary<string, int[]> cube = new()
            {
                //the dictionary, using the colors of ints[]. Every number in the unsorted list ints[] is here mapped to a position.
                {
                    "top",
                    new int[]
                    {
                        ints[33], ints[34], ints[35], ints[21], ints[22], ints[23], ints[9], ints[10], ints[11]
                    }
                },
                {
                    "bottom",
                    new int[]
                    {
                        ints[18], ints[19], ints[20], ints[30], ints[31], ints[32], ints[42], ints[43], ints[44]
                    }
                },
                {
                    "front",
                    new int[] { ints[0], ints[1], ints[2], ints[3], ints[4], ints[5], ints[6], ints[7], ints[8] }
                },
                {
                    "back",
                    new int[]
                    {
                        ints[47], ints[46], ints[45], ints[50], ints[49], ints[48], ints[53], ints[52], ints[51]
                    }
                },
                {
                    "left",
                    new int[]
                    {
                        ints[36], ints[24], ints[12], ints[38], ints[26], ints[14], ints[40], ints[28], ints[16]
                    }
                },
                {
                    "right",
                    new int[]
                    {
                        ints[13], ints[25], ints[37], ints[15], ints[27], ints[39], ints[17], ints[29], ints[41]
                    }
                }
            };

            return cube;
        }

        /// <summary>
        /// Helper method to PermCube(). Updates the solution based on permutations to the cube.
        /// </summary>
        /// <param name="perm">The permutation.</param>
        /// <param name="amount">The amount of CW turns. 1 for CW, 3 for CCW.</param>
        /// <param name="updateSolutionOptimality">Whether to update the solutionoptimality or not.</param>
        private void UpdateSolution(char perm, int amount, bool updateSolutionOptimality)
        {
            if (_solution.Count > 0 && _solution[0][0] == perm)
            {
                string newNextMove = new string(perm, Math.Abs(_solution[0].Length - amount));

                switch (newNextMove.Length)
                {
                    case 0:
                        _solution.RemoveAt(0);
                        break;
                    case 2:
                        _solution.RemoveAt(0);
                        _solution.Insert(0, new string(perm, 1));
                        _solution.Insert(0, new string(perm, 1));
                        break;
                    default:
                        _solution[0] = newNextMove;
                        break;
                }

                return;
            }

            if (!(_solution.Count == 0 && perm is 'x' or 'y' or 'z'))
            {
                if (amount != 2)
                    _solution.Insert(0, new string(perm, 4 - amount));
                else
                {
                    _solution.Insert(0, new string(perm, 1));
                    _solution.Insert(0, new string(perm, 1));
                }

                if (updateSolutionOptimality)
                    solutionOptimal = false;
            }
        }

        /// <summary>
        /// Does one CW or CCW permutation to the cube.
        /// </summary>
        /// <param name="cubeToPerm">The original cube</param>
        /// <param name="perm">he permutation.</param>
        /// <param name="amount">The amount of CW turns. 1 for CW, 3 for CCW.</param>
        /// <param name="updatePermsTextList">Whether to update the permslist (undo stack) or not.</param>
        /// <param name="updateSolution">Whether to update the solution or not.</param>
        /// <param name="updateSolutionOptimality">Whether to update the solutionoptimality or not.</param>
        /// <returns></returns>
        public int[,,] PermCube(int[,,] cubeToPerm, char perm, int amount, bool updatePermsTextList = true, bool updateSolution = true, bool updateSolutionOptimality = true) // this function rotates [amount] times in the clockwise rotation
        {
            int[,,] tempoCube = cubeToPerm;
            int subtractAmount;
            if (perm == 'L' || perm == 'F' || perm == 'U' || perm == 'l' || perm == 'f' || perm == 'u' || perm == 'y' || perm == 'z' || perm == 'M' || perm == 'S' )
            {
                subtractAmount = 4;
            }
            else
            {
                subtractAmount = 0;
            }
            for (int n = Math.Abs(subtractAmount - amount);
                 n > 0;
                 n--) //if n=1; rotation should be clockwise in the case of L, F and U. However the onePerm function rotates counterclockwise, thus run 3 times or 4-n times.
            {
               tempoCube = OnePerm(perm, tempoCube);
            }

            if (updatePermsTextList)
                _permsTextList.Add(new string(perm, amount));

            if (CubeOperations.CheckIfSolved(new Cube(tempoCube)))
                _solution.Clear();
            else if (updateSolution && SolutionKnown)
                UpdateSolution(perm, amount, updateSolutionOptimality);

            return tempoCube;
        }

        /// <summary>
        /// static permute method, containing 'illegal moves' for the PermCube, and is static as to comply with its usage in certain locations.
        /// </summary>
        /// <param name="cubeToPerm">the cube to permute</param>
        /// <param name="perm">permutation</param>
        /// <returns>permuted cube as int[,,]</returns>
        public static int[,,] SolvePerms(int[,,] cubeToPerm, char perm)
        {
            int[,,] tempoCube = cubeToPerm;
            int subtractAmount;
            if (perm == 'L' || perm == 'F' || perm == 'U' || perm == 'l' || perm == 'f' || perm == 'u' || perm == 'y' || perm == 'z' || perm == 'M' || perm == 'S' )
            {
                subtractAmount = 4;
            }
            else
            {
                subtractAmount = 0;
            }
            for (int n = Math.Abs(subtractAmount - 1);
                 n > 0;
                 n--) //if n=1; rotation should be clockwise in the case of L, F and U. However the onePerm function rotates counterclockwise, thus run 3 times or 4-n times.
            {
                tempoCube = OnePerm(perm, tempoCube);
            }
            return tempoCube;
        }
        
        /// <summary>
        /// this function does either one clockwise, or counterclockwise rotation, depending on the permutation executed. Therefore this function should remain private.
        /// </summary>
        /// <param name="perm">permutation</param>
        /// <param name="cubeToPerm">the cube to permute</param>
        /// <returns>permuted cube as int[,,]</returns>
        /// <exception cref="ArgumentException"></exception>
        private static int[,,] OnePerm(char perm, int[,,] cubeToPerm) 
        {
            int[,,] tempCube;
            Tuple<int, int, int, int, int> WhatRotationParams = WhatRotation(perm);
            int xRotInt  = WhatRotationParams.Item1;
            int yRotInt  = WhatRotationParams.Item2;
            int zRotInt  = WhatRotationParams.Item3;
            int beginVal = WhatRotationParams.Item4;
            int endVal   = WhatRotationParams.Item5;
            int lengthOfCube =
                cubeToPerm.GetLength(
                    0); //gets length of 1 dimension only, however it is a square so every length is the same


            /* permutations and over what axis they rotate:
             *F/F2/F' = xRot
             *B/B2/B' = xRot
             *U/U2/U' = yRot
             *D/D2/D' = yRot
             *L/L2/L' = zRot
             *R/R2/R' = zRot
             */

            tempCube = (int[,,]) cubeToPerm.Clone();

            for (int x = 0; x < lengthOfCube; x++) //first dimension in array
            {
                for (int y = 0; y < lengthOfCube; y++) //second dimension in array
                {
                    for (int z = 0; z < lengthOfCube; z++) //third dimension in array                  
                    {
                        /*for the allowed layers (the layer itself+ the faces layer next to it) {
                         *      transpose over the axes that are not the face axis
                         *      rotate over the axes that are not the face axis
                         *  }
                         * else {
                         * keep value in place
                         * }
                         */
                        if ((perm == 'F' && x <= endVal) || (perm == 'B' && x >= beginVal) ||
                            (perm == 'L' && z <= endVal) || (perm == 'R' && z >= beginVal) ||
                            (perm == 'U' && y <= endVal) || (perm == 'D' && y >= beginVal) ||
                            (perm == 'S' && x == beginVal) || (perm == 'M' && z == beginVal) || (perm == 'E' && y == beginVal) ||
                            (perm == 'f' && x <= endVal) || (perm == 'b' && x >= beginVal) ||
                            (perm == 'l' && z <= endVal) || (perm == 'r' && z >= beginVal) ||
                            (perm == 'u' && y <= endVal) || (perm == 'd' && y >= beginVal) ||
                            (perm == 'x') || (perm == 'y') || (perm == 'z'))
                        {
                            tempCube[(xRotInt * x + yRotInt * z + zRotInt * y),
                                    (xRotInt * z + yRotInt * y + zRotInt * x),
                                    (xRotInt * y + yRotInt * x + zRotInt * z)] =
                                cubeToPerm[Math.Abs((lengthOfCube - 1) * yRotInt - x),
                                    Math.Abs((lengthOfCube - 1) * zRotInt - y),
                                    Math.Abs((lengthOfCube - 1) * xRotInt - z)]; 
                        }
                        else
                        {
                            tempCube[x, y, z] = cubeToPerm[x, y, z];
                        }
                    }
                }
            }
            return tempCube;
        }

        /// <summary>
        /// helper method for OnePerm. Takes a perm, and returns tuple of dependent variable numbers
        /// </summary>
        /// <param name="perm">a perm</param>
        /// <returns>tuple (xRotInt, yRotInt, zRotInt, beginVal, endVal)</returns>
        /// <exception cref="ArgumentException"></exception>
        private static Tuple<int, int, int, int, int> WhatRotation(char perm)
        {
            int xRotInt = 0;
            int yRotInt = 0;
            int zRotInt = 0;
            int beginVal = 0;
            int endVal = 4;
            switch (perm)
            {
                case 'F':
                    xRotInt = 1;
                    endVal = 1;
                    break;
                case 'B':
                    xRotInt = 1;
                    beginVal = 3;
                    break;
                case 'U':
                    yRotInt = 1;
                    endVal = 1;
                    break;
                case 'D':
                    yRotInt = 1;
                    beginVal = 3;
                    break;
                case 'L':
                    zRotInt = 1;
                    endVal = 1;
                    break;
                case 'R':
                    zRotInt = 1;
                    beginVal = 3;
                    break;
                
                case 'S':
                    xRotInt = 1;
                    beginVal = 2;
                    break;
                case 'E':
                    yRotInt = 1;
                    beginVal = 2;
                    break;
                case 'M':
                    zRotInt = 1;
                    beginVal = 2;
                    break;
                
                case 'f':
                    xRotInt = 1;
                    endVal = 2;
                    break;
                case 'b':
                    xRotInt = 1;
                    beginVal = 2;
                    break;
                case 'u':
                    yRotInt = 1;
                    endVal = 2;
                    break;
                case 'd':
                    yRotInt = 1;
                    beginVal = 2;
                    break;
                case 'l':
                    zRotInt = 1;
                    endVal = 2;
                    break;
                case 'r':
                    zRotInt = 1;
                    beginVal = 2;
                    break;
                case 'x':
                    zRotInt = 1;
                    break;
                case 'y':
                    yRotInt = 1;
                    break;
                case 'z':
                    xRotInt = 1;
                    break;
                
                default:
                    throw new ArgumentException("Invalid perm");
            }
            return Tuple.Create(xRotInt, yRotInt, zRotInt, beginVal, endVal);
        }
        
        /// <summary>
        /// method to undo a move
        /// </summary>
        /// <param name="window">window</param>
        public void UndoMove(MainWindow window)
        {
            if (_permsTextList.Count == 0)
                return;

            string lastPerm = _permsTextList[^1];
            window.RotateFace(lastPerm[0], 4 - lastPerm.Length, false);

            int maxIndex = _permsTextList.Count - 1;

            for (int i = maxIndex; i > maxIndex - 2; i--)
            {
                _permsTextList.RemoveAt(i);
            }

            RedoStack.Add(lastPerm);
        }

        /// <summary>
        /// method to redo a move
        /// </summary>
        /// <param name="window">window</param>
        public void RedoMove(MainWindow window)
        {
            if (RedoStack.Count == 0)
                return;

            string perm = RedoStack[^1];
            RedoStack.RemoveAt(RedoStack.Count - 1);
            window.RotateFace(perm[0], perm.Length, false);
        }

        /// <summary>
        /// method to clone a cube
        /// </summary>
        /// <returns>a cube</returns>
        public Cube Clone()
        {
            Cube c = new Cube();
            c.Values = this.Values;
            c._permsTextList = this._permsTextList.ToList();
            c._solution = this.Solution.ToList();
            c.solutionOptimal = this.solutionOptimal;

            return c;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="_dictionary"></param>
        /// <returns></returns>
        public static Cube Parse(Dictionary<string, int[]> _dictionary)
        {
            int[,,] values = new int[5, 5, 5];
            /*
             * color pieces to import to:
             * front:   [0,1,1], [0,1,2], [0,1,3], [0,2,1], [0,2,2], [0,2,3], [0,3,0], [0,3,1], [0,3,2]
             * back:    [4,3,3], [4,3,2], [4,3,1], [4,2,3], [4,2,2], [4,2,1], [4,1,3], [4,2,2], [4,2,1]
             * left:    [1,1,0], [2,1,0], [3,1,0], [1,2,0], [2,2,0], [3,2,0], [1,3,0], [2,3,0], [3,3,0],
             * right:   [3,2,4], [2,2,4], [1,2,4], [3,3,4], [2,3,4], [1,3,4], [3,4,4], [2,4,4], [1,4,4],
             * top:      [1,0,1], [1,0,2], [1,0,3], [2,0,1], [2,0,2], [2,0,3], [3,0,1], [3,0,2], [3,0,3], 
             * bottom:    [1,4,1], [1,4,2], [1,4,3], [2,4,1], [2,4,2], [2,4,3], [3,4,1], [3,4,2], [3,4,3],
             * middle pieces to match to color:
             * xxx
             * edge pieces to match to color:
             * yyy
             * corner pieces to match to color:
             */
            //first, assign the right color values to the cube
            //front
            values[0, 1, 1] = _dictionary["front"][0];
            values[0, 1, 2] = _dictionary["front"][1];
            values[0, 1, 3] = _dictionary["front"][2];
            values[0, 2, 1] = _dictionary["front"][3];
            values[0, 2, 2] = _dictionary["front"][4];
            values[0, 2, 3] = _dictionary["front"][5];
            values[0, 3, 1] = _dictionary["front"][6];
            values[0, 3, 2] = _dictionary["front"][7];
            values[0, 3, 3] = _dictionary["front"][8];
            //back
            values[4, 1, 3] = _dictionary["back"][0];
            values[4, 1, 2] = _dictionary["back"][1];
            values[4, 1, 1] = _dictionary["back"][2];
            values[4, 2, 3] = _dictionary["back"][3];
            values[4, 2, 2] = _dictionary["back"][4];
            values[4, 2, 1] = _dictionary["back"][5];
            values[4, 3, 3] = _dictionary["back"][6];
            values[4, 3, 2] = _dictionary["back"][7];
            values[4, 3, 1] = _dictionary["back"][8];
            //left
            values[3, 1, 0] = _dictionary["left"][0];
            values[2, 1, 0] = _dictionary["left"][1];
            values[1, 1, 0] = _dictionary["left"][2];
            values[3, 2, 0] = _dictionary["left"][3];
            values[2, 2, 0] = _dictionary["left"][4];
            values[1, 2, 0] = _dictionary["left"][5];
            values[3, 3, 0] = _dictionary["left"][6];
            values[2, 3, 0] = _dictionary["left"][7];
            values[1, 3, 0] = _dictionary["left"][8];
            //right
            values[1, 1, 4] = _dictionary["right"][0];
            values[2, 1, 4] = _dictionary["right"][1];
            values[3, 1, 4] = _dictionary["right"][2];
            values[1, 2, 4] = _dictionary["right"][3];
            values[2, 2, 4] = _dictionary["right"][4];
            values[3, 2, 4] = _dictionary["right"][5];
            values[1, 3, 4] = _dictionary["right"][6];
            values[2, 3, 4] = _dictionary["right"][7];
            values[3, 3, 4] = _dictionary["right"][8];
            //top
            values[3, 0, 1] = _dictionary["top"][0];
            values[3, 0, 2] = _dictionary["top"][1];
            values[3, 0, 3] = _dictionary["top"][2];
            values[2, 0, 1] = _dictionary["top"][3];
            values[2, 0, 2] = _dictionary["top"][4];
            values[2, 0, 3] = _dictionary["top"][5];
            values[1, 0, 1] = _dictionary["top"][6];
            values[1, 0, 2] = _dictionary["top"][7];
            values[1, 0, 3] = _dictionary["top"][8];
            //bottom
            values[1,4,1] = _dictionary["bottom"][0];
            values[1,4,2] = _dictionary["bottom"][1];
            values[1,4,3] = _dictionary["bottom"][2];
            values[2,4,1] = _dictionary["bottom"][3];
            values[2,4,2] = _dictionary["bottom"][4];
            values[2,4,3] = _dictionary["bottom"][5];
            values[3,4,1] = _dictionary["bottom"][6];
            values[3,4,2] = _dictionary["bottom"][7];
            values[3,4,3] = _dictionary["bottom"][8];
            
            //EDGE MAPPING:
            /* edge piece positions, matched to relevant positions:
             * cubie    = face1Position, face2Position
             * [1,1,2]  = [0,1,2], [1,0,2]
             * [1,2,1]  = [0,2,1], [1,2,0]
             * [1,2,3]  = [0,2,3], [1,2,4]
             * [1,3,2]  = [0,3,2], [1,4,2]
             * [2,1,1]  = [2,1,0], [2,0,1]
             * [2,1,3]  = [2,0,3], [2,1,4]
             * [2,3,1]  = [2,3,0], [2,4,1]
             * [2,3,3]  = [2,3,4], [2,4,3]
             * [3,1,2]  = [4,1,2], [3,0,2]
             * [3,2,1]  = [4,2,1], [3,2,0]
             * [3,2,3]  = [4,2,3], [3,2,4]
             * [3,3,2]  = [4,3,2], [3,4,2]
             * edge piece color pairings:
             * cubie = colors, color    = [colorInt1,colorInt2] || [colorInt2,colorInt1]
             * 8    = White,    Red     = [1,6] || [6,1]
             * 10   = Green,    Red     = [3,6] || [6,3]
             * 12   = Blue,     Red     = [2,6] || [6,2]
             * 14   = Yellow,   Red     = [4,6] || [6,4]
             * 16   = White,    Green   = [1,3] || [3,1]
             * 18   = White,    Blue    = [1,2] || [2,1]
             * 22   = Green,    Yellow  = [3,4] || [4,3]
             * 24   = Blue,     Yellow  = [2,4] || [4,2]
             * 26   = White,    Orange  = [1,5] || [5,1]
             * 28   = Green,    Orange  = [3,5] || [5,3]
             * 30   = Blue,     Orange  = [2,5] || [5,2]
             * 32   = Yellow,   Orange  = [4,5] || [5,4]
             */
            
            //dictionary to map cubie positions to face positions. Does not say anything about the cubie value in that position, just the face positions that influence the cubie in that position
            //key, cubie, face1, face2
            Dictionary<Tuple<int, int, int>, Tuple<Tuple<int, int, int>, Tuple<int, int, int>>> _EdgeCubieToFacesPos = new Dictionary<Tuple<int, int, int>, Tuple<Tuple<int, int, int>, Tuple<int, int, int>>>()
            {
                {Tuple.Create(1,1,2), Tuple.Create(Tuple.Create(0,1,2), Tuple.Create(1,0,2))},
                {Tuple.Create(1,2,1), Tuple.Create(Tuple.Create(0,2,1), Tuple.Create(1,2,0))},
                {Tuple.Create(1,2,3), Tuple.Create(Tuple.Create(0,2,3), Tuple.Create(1,2,4))},
                {Tuple.Create(1,3,2), Tuple.Create(Tuple.Create(0,3,2), Tuple.Create(1,4,2))},
                {Tuple.Create(2,1,1), Tuple.Create(Tuple.Create(2,1,0), Tuple.Create(2,0,1))},
                {Tuple.Create(2,1,3), Tuple.Create(Tuple.Create(2,0,3), Tuple.Create(2,1,4))},
                {Tuple.Create(2,3,1), Tuple.Create(Tuple.Create(2,3,0), Tuple.Create(2,4,1))},
                {Tuple.Create(2,3,3), Tuple.Create(Tuple.Create(2,3,4), Tuple.Create(2,4,3))},
                {Tuple.Create(3,1,2), Tuple.Create(Tuple.Create(4,1,2), Tuple.Create(3,0,2))},
                {Tuple.Create(3,2,1), Tuple.Create(Tuple.Create(4,2,1), Tuple.Create(3,2,0))},
                {Tuple.Create(3,2,3), Tuple.Create(Tuple.Create(4,2,3), Tuple.Create(3,2,4))},
                {Tuple.Create(3,3,2), Tuple.Create(Tuple.Create(4,3,2), Tuple.Create(3,4,2))}
            };
            //dictionary that maps cubies to the corresponding colors. Does not say anything about position, only value
            Dictionary<int, Tuple<int, int>> _edgeCubieToColors = new Dictionary<int, Tuple<int, int>>()
            {
                { 8, Tuple.Create(1,6) },
                {10, Tuple.Create(3,6) },
                {12, Tuple.Create(2,6) },
                {14, Tuple.Create(4,6) },
                {16, Tuple.Create(1,3) },
                {18, Tuple.Create(1,2) },
                {22, Tuple.Create(3,4) },
                {24, Tuple.Create(2,4) },
                {26, Tuple.Create(1,5) },
                {28, Tuple.Create(3,5) },
                {30, Tuple.Create(2,5) },
                {32, Tuple.Create(4,5) },
            };
            
            foreach (var _cubiePos in _EdgeCubieToFacesPos.Keys)
            {
                foreach (var _cubieValue in _edgeCubieToColors.Keys)
                {
                    if (( values[_EdgeCubieToFacesPos[_cubiePos].Item1.Item1, _EdgeCubieToFacesPos[_cubiePos].Item1.Item2, _EdgeCubieToFacesPos[_cubiePos].Item1.Item3] == _edgeCubieToColors[_cubieValue].Item1    &&
                          values[_EdgeCubieToFacesPos[_cubiePos].Item2.Item1, _EdgeCubieToFacesPos[_cubiePos].Item2.Item2, _EdgeCubieToFacesPos[_cubiePos].Item2.Item3] == _edgeCubieToColors[_cubieValue].Item2 )  ||
                        ( values[_EdgeCubieToFacesPos[_cubiePos].Item1.Item1, _EdgeCubieToFacesPos[_cubiePos].Item1.Item2, _EdgeCubieToFacesPos[_cubiePos].Item1.Item3] == _edgeCubieToColors[_cubieValue].Item2    &&
                          values[_EdgeCubieToFacesPos[_cubiePos].Item2.Item1, _EdgeCubieToFacesPos[_cubiePos].Item2.Item2, _EdgeCubieToFacesPos[_cubiePos].Item2.Item3] == _edgeCubieToColors[_cubieValue].Item1 ))
                    {
                        values[_cubiePos.Item1, _cubiePos.Item2, _cubiePos.Item3] = _cubieValue;
                    }
                }
            }
            
            //CORNER MAPPING
            Dictionary<Tuple<int, int, int>, Tuple<Tuple<int, int, int>, Tuple<int, int, int>, Tuple<int, int, int>>> _CornerCubieToFacesPos = new Dictionary<Tuple<int, int, int>, Tuple<Tuple<int, int, int>, Tuple<int, int, int>, Tuple<int, int, int>>>()
            {
                {Tuple.Create(1,1,1), Tuple.Create(Tuple.Create(0,1,1), Tuple.Create(1,0,1), Tuple.Create(1,1,0))},
                {Tuple.Create(1,1,3), Tuple.Create(Tuple.Create(0,1,3), Tuple.Create(1,0,3), Tuple.Create(1,1,4))},
                {Tuple.Create(1,3,1), Tuple.Create(Tuple.Create(0,3,1), Tuple.Create(1,4,1), Tuple.Create(1,3,0))},
                {Tuple.Create(1,3,3), Tuple.Create(Tuple.Create(0,3,3), Tuple.Create(1,4,3), Tuple.Create(1,3,4))},
                {Tuple.Create(3,1,1), Tuple.Create(Tuple.Create(4,1,1), Tuple.Create(3,0,1), Tuple.Create(3,1,0))},
                {Tuple.Create(3,1,3), Tuple.Create(Tuple.Create(4,1,3), Tuple.Create(3,0,3), Tuple.Create(3,1,4))},
                {Tuple.Create(3,3,1), Tuple.Create(Tuple.Create(4,3,1), Tuple.Create(3,4,1), Tuple.Create(3,3,0))},
                {Tuple.Create(3,3,3), Tuple.Create(Tuple.Create(4,3,3), Tuple.Create(3,4,3), Tuple.Create(3,3,4))},
            };
            Dictionary<int, Tuple<int, int, int>> _CornerCubieToColors = new Dictionary<int, Tuple<int, int, int>>()
            {
                {  7, Tuple.Create(6,1,3) },
                {  9, Tuple.Create(6,1,2) },
                { 13, Tuple.Create(6,4,3) },
                { 15, Tuple.Create(6,4,2) },
                { 25, Tuple.Create(5,1,3) },
                { 27, Tuple.Create(5,1,2) },
                { 31, Tuple.Create(5,4,3) },
                { 33, Tuple.Create(5,4,2) },
            };
            foreach (var _cubiePos in _CornerCubieToFacesPos.Keys)
            {
                foreach (var _cubieValue in _CornerCubieToColors.Keys)
                {
                    if (( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item1    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item2    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item3 )  ||
                        ( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item1    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item3    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item2 )  ||
                        ( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item2    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item1    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item3 )  ||
                        ( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item2    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item3    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item1 )  ||
                        ( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item3    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item1    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item2 )  ||
                        ( values[_CornerCubieToFacesPos[_cubiePos].Item1.Item1, _CornerCubieToFacesPos[_cubiePos].Item1.Item2, _CornerCubieToFacesPos[_cubiePos].Item1.Item3] == _CornerCubieToColors[_cubieValue].Item3    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item2.Item1, _CornerCubieToFacesPos[_cubiePos].Item2.Item2, _CornerCubieToFacesPos[_cubiePos].Item2.Item3] == _CornerCubieToColors[_cubieValue].Item2    &&
                          values[_CornerCubieToFacesPos[_cubiePos].Item3.Item1, _CornerCubieToFacesPos[_cubiePos].Item3.Item2, _CornerCubieToFacesPos[_cubiePos].Item3.Item3] == _CornerCubieToColors[_cubieValue].Item1 )
                        )
                    {
                        values[_cubiePos.Item1, _cubiePos.Item2, _cubiePos.Item3] = _cubieValue;
                    }
                }
            }
            
            //CENTER CUBE MAPPING
            Dictionary<Tuple<int, int, int>, Tuple<int, int, int>> _CentreFaceToCubiePos =
                new Dictionary<Tuple<int, int, int>, Tuple<int, int, int>>()
                {
                    {Tuple.Create(1,2,2), Tuple.Create(0,2,2) },
                    {Tuple.Create(2,1,2), Tuple.Create(2,0,2) },
                    {Tuple.Create(2,2,1), Tuple.Create(2,2,0) },
                    {Tuple.Create(2,2,3), Tuple.Create(2,2,4) },
                    {Tuple.Create(2,3,2), Tuple.Create(2,4,2) },
                    {Tuple.Create(3,2,2), Tuple.Create(4,2,2) },
                };
            Dictionary<int, int> _CentreFaceColorToCubieVal = new Dictionary<int, int>()
            {
                {1,17},
                {2,21},
                {3,19},
                {4,23},
                {5,29},
                {6,11},
            };
            foreach (var _centreCubiepos in _CentreFaceToCubiePos)
            {
                foreach (var _centrecubieVal in _CentreFaceColorToCubieVal)
                {
                    if (values[_centreCubiepos.Value.Item1, _centreCubiepos.Value.Item2, _centreCubiepos.Value.Item3] == _centrecubieVal.Key)
                    {
                        values[_centreCubiepos.Key.Item1, _centreCubiepos.Key.Item2, _centreCubiepos.Key.Item3] = _centrecubieVal.Value;
                    }
                }
            }
            values[2, 2, 2] = 20; //centre most cubie does not change location
            
            Cube c = new Cube(values);
            return c;
        }
    }
}
