using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace rubikscube
{
    public class Backend
    {
        /// <summary>
        /// GenerateMD5 generates an MD5 hash based on an input string
        /// </summary>
        /// <param name="input">an input string</param>
        /// <returns>MD5 hash</returns>
        public static string GenerateMd5(string input)
        {
            //Source: https://stackoverflow.com/questions/11454004/calculate-a-md5-hash-from-a-string
            if (input == "") // if input seed is empty
            {
                return "";
            }
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                return Convert.ToHexString(hashBytes);
            }
        }
        
        /// <summary>
        /// GeneratePermsFromMd5 takes a MD5 hash as an input, and returns a string of perms
        /// </summary>
        /// <param name="input">an md5 hash</param>
        /// <returns>string of perms</returns>
        public static string GeneratePermsFromMd5(string input)
        {
            /*use rotation sequence F-L-U-B-R-D
             * every character c rotates 1 side a number of times, namely
             * asciiwvalue(c) % 3 + 1
             * (4 rotations are back to regular; thus useless)
             * the position of a character decides what face gets rotated
             **/
            char[] permOptions = { 'F', 'L', 'U', 'B', 'R', 'D' };
            byte[] asciiNumbers = Encoding.ASCII.GetBytes(input);
            string perms = "";
            for (int i = 0; i < asciiNumbers.Length; i++)
            {
                for (int j = 0; j < asciiNumbers[i] % 3 + 1; j++)
                {
                    perms += permOptions[i % 6].ToString();
                }
            }

            return perms;
        }
    }
}