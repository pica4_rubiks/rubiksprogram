﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace rubikscube;

public static class Solver
{
    //CFOP solver.

    /// <summary>
    /// core solve funtion. Takes a cube as an input, and returns the perms to a solved cube as an output
    /// </summary>
    /// <param name="input">the cube to be solved</param>
    /// <returns>list of perms, written as strings</returns>
    public static List<string> TheSolver(Cube input)
    {
        List<string> permsToSolved = new List<string>();
        Tuple<Cube, List<string>> getOnTopBase = getOnTop(input, 1, 6); //1 is white, 6 is red
        Tuple<Cube, List<string>> f2L = F2L(getOnTopBase.Item1);
        Tuple<Cube, List<string>> getOnTopF2L = getOnTop(f2L.Item1, 4, 2, true); //4 is yellow
        Tuple<Cube, List<string>> oll = OLL(getOnTopF2L.Item1);
        Tuple<Cube, List<string>> pll = PLL(oll.Item1);
            foreach (var perm in getOnTopBase.Item2)
            {
                permsToSolved.Add(perm);
            }
            foreach (var perm in f2L.Item2)
            {
                permsToSolved.Add(perm);
            }
            foreach (var perm in getOnTopF2L.Item2)
            {
                permsToSolved.Add(perm);
            }
            foreach (var perm in oll.Item2)
            {
                permsToSolved.Add(perm);
            }
            foreach (var perm in pll.Item2)
            {
                permsToSolved.Add(perm);
            }

        if (permsToSolved.Count == 0)
        {
            throw new Exception("cube is already solved");
        }
        
        return permsToSolved;
    }
     
    /// <summary>
    /// the F2l part of solving. Takes a cube as an input, and a tuple containing both the cube on which the perms have been performed, as well as the perms as a list of strings
    /// </summary>
    /// <param name="inputCube">a cube object</param>
    /// <returns>tuple of a cube and a list of strings</returns>
    private static Tuple<Cube, List<string>> F2L(Cube inputCube)
    {
        
        Cube newCube = new();
        newCube.Values = inputCube.Values;
        Cube outputCube = inputCube;

        if (outputCube.Values.Cast<int>().SequenceEqual(Cube.DefaultCube().Cast<int>())) // check if the cube is already solved
        {
            return new Tuple<Cube, List<string>>(inputCube, new List<string>{""});
        }
        var d =  JsonConvert.SerializeObject((Cube.DefaultCube())); // Make a deep copy (change the memory pointer) of the initial input cube in order to not change it when executing the method MaskString on it
        int[,,] defaultcube = JsonConvert.DeserializeObject<int[,,]>(d);

        // This following part executes the 3 stages of solving the first 2 layers I wanted to put this in a seperate method but it would be too many input variables for the amount of space saved.
        string PartialSolve = SolveAndMask(new int[] { 7, 8, 9, 16, 17, 18, 25, 26, 27 }, defaultcube, newCube.Values); // Here it calculates the first solve that only makes the first layer correct.
        string TotalSolve =  PartialSolve;
        newCube.Values = ExecutePerms(PartialSolve, newCube.Values);
        PartialSolve = SolveAndMask(new int[] { 7, 8, 9, 16, 17, 18, 25, 26, 27, 10, 11, 12, 21 }, defaultcube,newCube.Values); // These values correspond with the Cross being solved.
        TotalSolve += PartialSolve;
        newCube.Values = ExecutePerms(PartialSolve, newCube.Values);
        PartialSolve = SolveAndMask(new int[] { 7, 8, 9, 16, 17, 18, 25, 26, 27, 10, 11, 12, 21, 19, 28, 29, 30 }, defaultcube, newCube.Values); // This is the last step and solves the whole first 2 layers.
        TotalSolve += PartialSolve;
        newCube.Values = ExecutePerms(PartialSolve, newCube.Values);

     return new Tuple<Cube, List<string>>(newCube, MakeStringArray(TotalSolve));

    }
  

    /// <summary>
    /// the OLL part of solving. Takes a cube as an input, and a tuple containing both the cube on which the perms have been performed, as well as the perms as a list of strings
    /// </summary>
    /// <param name="inputCube">a cube object</param>
    /// <returns>tuple of a cube and a list of strings</returns>
    private static Tuple<Cube, List<string>> OLL(Cube inputCube)
    {
        Cube outputCube = inputCube;
        List<string> PermsString = new List<string>();
        
        if (outputCube.Values.Cast<int>().SequenceEqual(Cube.DefaultCube().Cast<int>()))
        {
            return new Tuple<Cube, List<string>>(outputCube, PermsString);
        }
        foreach (var Mask in PermAlgs.OLLMask)
        {
            Tuple<bool, int, List<string>> _DoesSequenceEqual = DoesOLLEqual(outputCube, Mask); //contains wether or not outputcube and mask are equal, and how many times that value is rotated
            if (_DoesSequenceEqual.Item1) 
            {

                foreach (var yrot in _DoesSequenceEqual.Item3)
                {
                    PermsString.Add(yrot);
                }
                foreach (var perm in PermAlgs.ParsePerms(Mask.Key))
                {
                    char charPerm = perm.ToCharArray()[0];
                    outputCube.Values = Cube.SolvePerms(outputCube.Values, charPerm); //manipulate the cube
                    PermsString.Add(perm);                                            //add to the string
                }

                break;
            }
        }
        return new Tuple<Cube, List<string>>(outputCube, PermsString);
    }

    /// <summary>
    /// method used to make sure the right color is on top for the next solving alg
    /// </summary>
    /// <param name="inputCube">cube to apply checks to</param>
    /// <param name="topColor">integer value of the desired top color</param>
    /// <param name="frontColor">integer value of the desired front color</param>
    /// <param name="disableFrontColor">disables the rotations to get the front color on the front</param>
    /// <returns>tuple of cube and perms</returns>
    private static Tuple<Cube, List<string>> getOnTop(Cube inputCube, int topColor, int frontColor, bool disableFrontColor = false)
    {
        Cube outputCube = inputCube;
        List<string> PermsString = new List<string>();
        if (outputCube.Values.Cast<int>().SequenceEqual(Cube.DefaultCube().Cast<int>()))
        {
            return new Tuple<Cube, List<string>>(outputCube, PermsString);
        }
        int rotateAxis = 0;
        
        while (outputCube.Values[2, 0, 2] != topColor)
        {
            if (rotateAxis == 4)
            {
                outputCube.Values = Cube.SolvePerms(outputCube.Values, 'y');
                PermsString.Add("y");
            }
            else
            {
                outputCube.Values = Cube.SolvePerms(outputCube.Values, 'x');
                PermsString.Add("x");
            }
            rotateAxis++;
        }

        if (!disableFrontColor)
        {
            while (outputCube.Values[0,2,2] != frontColor)
            {
                outputCube.Values = Cube.SolvePerms(outputCube.Values, 'y');
                PermsString.Add("y");
            }
        }
        
        return Tuple.Create(outputCube, PermsString);
    }
    /// <summary>
    /// This method puts any string into a string array ready to be returned into the tupple
    /// </summary>
    /// <param name="Solve">The string that gets put into String List</param>
    /// <returns></returns>
    private static List<string> MakeStringArray(string Solve)
    {
        List<string> permsString = new();
        foreach (var v in Solve)
        {
            permsString.Add(v.ToString());
        }

        return permsString;
    }
    /// <summary>
    /// This method saves a lot of space of having to manually set variables each time you solve. 
    /// </summary>
    /// <param name="MaskINTS"> All the cubies that dont have to be manipulated to -1 </param>
    /// <param name="defaultCube">The standard cube that is used to solve towards</param>
    /// <param name="newCube"> The scrambled cube that is used to solve with</param>
    /// <returns>The Solve in a string format of the particular solve stage</returns>
    private static string SolveAndMask(int[] MaskINTS, int[,,] defaultCube, int[,,] newCube)
    {
        return Solving(MasktheStringCube(newCube, MaskINTS), MasktheStringCube(defaultCube, MaskINTS));
    }
    /// <summary>
    /// This method executes perms based on the input string.
    /// </summary>
    /// <param name="Solve"> Permutations to do in string form </param>
    /// <param name="CurrentCube"> Current 3d INT to do permutations on</param>
    /// <returns>A 3d 5x5x5 int containing the cube after the permutations were executed on it. </returns>
    private static int[,,] ExecutePerms(string Solve, int[,,] CurrentCube)
    {
        foreach (char c in Solve)
        {
            CurrentCube = Cube.SolvePerms(CurrentCube, c);
        }

        return CurrentCube;
    }
    /// <summary>
    /// The PLL part of solving. Takes a cube as an input, and a tuple containing both the cube on which the perms have been performed, as well as the perms as a list of strings
    /// </summary>
    /// <param name="inputCube">a cube object</param>
    /// <returns>tuple of a cube and a list of strings</returns>
    private static Tuple<Cube, List<string>> PLL(Cube inputCube)
    {
        Cube outputCube = inputCube;
        List<string> PermsString = new List<string>();
        
        if (outputCube.Values.Cast<int>().SequenceEqual(Cube.DefaultCube().Cast<int>()))
        {
            return new Tuple<Cube, List<string>>(outputCube, PermsString);
        }

        foreach (var CubeState in PermAlgs.PLLStates)
        {
            Tuple<bool, int, int, List<string>> _DoesSequenceEqual = DoesPLLEqual(outputCube, CubeState); //contains wether or not they are equal, what item in the value tuple it is, and how many times that value is rotated
            if (_DoesSequenceEqual.Item1) 
            {
                foreach (var yrot in _DoesSequenceEqual.Item4)
                {
                    PermsString.Add(yrot);
                }
                foreach (var perm in PermAlgs.ParsePerms(CubeState.Key))
                {
                    char charPerm = perm.ToCharArray()[0];
                    outputCube.Values = Cube.SolvePerms(outputCube.Values, charPerm); //manipulate the cube
                    PermsString.Add(perm);                                            //add to the string
                }

                while (outputCube.Values[0,2,2] != outputCube.Values[0,1,2]) //final u rotation
                {
                    outputCube.Values = Cube.SolvePerms(outputCube.Values, 'U'); //manipulate the cube
                    PermsString.Add("U");
                }
                break;
            }
        }
        return new Tuple<Cube, List<string>>(outputCube, PermsString);
    }

    /// <summary>
    /// checks wether or not the inputCube only contains yellow (4), corresponding to the mask of a subalgorithm (example: H(2)). Checks all 4 rotations
    /// </summary>
    /// <param name="inputCube1">the cube to check</param>
    /// <param name="mask">the mask to apply</param>
    /// <returns>uple containing a bool wether or not they equal, and the amount of rotations</returns>
    private static Tuple<bool, int, List<string>> DoesOLLEqual(Cube inputCube1, KeyValuePair<string, int[,,]> mask)
    {
        List<string> PermsString = new List<string>();
        
        for (int HowManyRots = 0; HowManyRots < 4; HowManyRots++)
        {
            inputCube1.Values = Cube.SolvePerms(inputCube1.Values, 'y'); //rotate once per cycle
            PermsString.Add("y");
            if (MaskTrue(inputCube1.Values, mask.Value, 4)) //4 is yellow
            {
                return Tuple.Create(true, (HowManyRots + 1) % 4, PermsString);
            }
        }

        return Tuple.Create(false, 1, PermsString);
    }
    
    /// <summary>
    /// checks wether or not the inputCube is equal to one of the 4 variants of a subalgorithm (example: U(a)). Also checks 4 rotations, giving a total of 16 checks
    /// </summary>
    /// <param name="inputCube1">the cube to check</param>
    /// <param name="inputCube2">the dictionary containing 4 variants of a subalgorithm</param>
    /// <returns>Tuple containing a bool wether or not they equal, what item of inputCube2 value, and the amount of rotations from matching value (for solving; take 4-rotations)</returns>
    private static Tuple<bool, int, int,  List<string>> DoesPLLEqual(Cube inputCube1, KeyValuePair<string, Tuple<int[,,], int[,,], int[,,], int[,,]>> inputCube2)
    {
        List<string> PermsString = new List<string>();

        for (int HowManyRots = 0; HowManyRots < 4; HowManyRots++) //voor elk van de 3 orientaties
        {
            inputCube1.Values = Cube.SolvePerms(inputCube1.Values, 'y'); //rotate once per cycle
            PermsString.Add("y");
            
            if (ApplyMask(inputCube1.Values, PermAlgs.PLLMask["PLLMask"]).Cast<int>().SequenceEqual(ApplyMask(inputCube2.Value.Item1, PermAlgs.PLLMask["PLLMask"]) .Cast<int>()))
            {
                return Tuple.Create(true, 1, (HowManyRots + 1) % 4, PermsString);
            }
            else if (ApplyMask(inputCube1.Values, PermAlgs.PLLMask["PLLMask"]).Cast<int>().SequenceEqual(ApplyMask(inputCube2.Value.Item2, PermAlgs.PLLMask["PLLMask"]) .Cast<int>()))
            {
                return Tuple.Create(true, 2, (HowManyRots + 1) % 4, PermsString);
            }
            else if (ApplyMask(inputCube1.Values, PermAlgs.PLLMask["PLLMask"]).Cast<int>().SequenceEqual(ApplyMask(inputCube2.Value.Item3, PermAlgs.PLLMask["PLLMask"]) .Cast<int>()))
            {
                return Tuple.Create(true, 3, (HowManyRots + 1) % 4, PermsString);
            }
            else if (ApplyMask(inputCube1.Values, PermAlgs.PLLMask["PLLMask"]).Cast<int>().SequenceEqual(ApplyMask(inputCube2.Value.Item4, PermAlgs.PLLMask["PLLMask"]) .Cast<int>()))
            {
                return Tuple.Create(true, 4, (HowManyRots + 1) % 4, PermsString);
            }
        }
        return Tuple.Create(false, 0, 0, PermsString);
    }




    /// <summary>
    /// A method that returns a boolean value depending on wether the remaining locations in array are equal to maskRemainder or not
    /// </summary>
    /// <param name="cubeToCheck">the cube to check</param>
    /// <param name="mask">the mask to apply</param>
    /// <param name="maskRemainder">the remainder integer. (example: yellow = 4)</param>
    /// <returns></returns>
    private static bool MaskTrue(int[,,] cubeToCheck,  int[,,] mask, int maskRemainder)
    {
        int[,,] outputCube = ApplyMask(cubeToCheck, mask);
        int sumOfMaskRemainder = 9*maskRemainder;
        for (int i = 0; i < cubeToCheck.GetLength(0); i++) //outer most array length
        {
            for (int j = 0; j < cubeToCheck.GetLength(1); j++) //middle most array length
            {
                for (int k = 0; k < cubeToCheck.GetLength(2); k++) //inner most array length
                {
                    if (outputCube[i, j, k] != 0 && outputCube[i, j, k] == maskRemainder) //if face is not maskRemainder, or masked
                    {
                        sumOfMaskRemainder -= maskRemainder;
                    }

                }
            }
        }

        if (sumOfMaskRemainder != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// helper function that applies mask to cube
    /// </summary>
    /// <param name="cube">cube to perm</param>
    /// <param name="mask">mask to apply</param>
    /// <returns>masked cube</returns>
    private static int[,,] ApplyMask(int[,,] cube, int[,,] mask)
    {
        int[,,] outputCube = new int[5, 5, 5];
        for (int i = 0; i < cube.GetLength(0); i++) //outer most array length
        {
            for (int j = 0; j < cube.GetLength(1); j++) //middle most array length
            {
                for (int k = 0; k < cube.GetLength(2); k++) //inner most array length
                {
                    outputCube[i, j, k] = cube[i, j, k] * mask[i, j, k];
                    
                }
            }
        }

        return outputCube;
    } /// <summary>
    /// this method uses  an int array to put certain cubies within the 3d int to the value -1 which later gets put to X because the blocks connected don't matter. It 
    /// </summary>
    /// <param name="CurrentCube"> the current string cube</param>
    /// <param name="MaskINTS"> the cubies to remain after they others are set to -1</param>
    /// <returns></returns>
    public static string MasktheStringCube(int[,,] CurrentCube, int[] MaskINTS)
    {
        var v = JsonConvert.SerializeObject((CurrentCube));

        int[,,] DeepCopyCube = JsonConvert.DeserializeObject<int[,,]>(v);
        for (int a = 0; a < 5; a++) //add the colors
        {
            for (int b = 0; b < 5; b++)
            {
                for (int c = 0; c < 5; c++)
                {

                    if (MaskINTS.Contains(DeepCopyCube[a, b, c]) == false && DeepCopyCube[a, b, c] > 6 && DeepCopyCube[a, b, c] != 0)
                    {

                        for (int e = -1; e < 2; e += 2)
                        {
                            if (DeepCopyCube[a + e, b, c] < 7 && DeepCopyCube[a + e, b, c] != 0)
                            {
                                try
                                {
                                    DeepCopyCube[a + e, b, c] = -1;
                                }
                                catch
                                {
                                }
                            }

                            if (DeepCopyCube[a, b + e, c] < 7 && DeepCopyCube[a + e, b, c] != 0)
                            {
                                try
                                {
                                    DeepCopyCube[a, b + e, c] = -1;
                                }
                                catch
                                {
                                }
                            }

                            if (DeepCopyCube[a, b, c + e] < 7 && DeepCopyCube[a + e, b, c] != 0)
                            {
                                try
                                {
                                    DeepCopyCube[a, b, c + e] = -1;
                                }
                                catch
                                {
                                }
                            }
                        }



                    }
                }
            }
        }

        return ToStringCube(DeepCopyCube);
    }
    /// <summary>
    ///  This method transforms the 3d int cube into a string with the correct sequence.
    /// </summary>
    /// <param name="currentCube"> The cube that has to be converted into a string based cube</param>
    /// <returns> A string cube that is transformed from the inputted 3d int</returns>
    public static string ToStringCube(int[,,] currentCube)
    {

        List<int> intArrayCube = new();
        for (int a = 0; a < 5; a++) 
        {
            for (int b = 0; b < 5; b++)
            {
                for (int c = 0; c < 5; c++)
                {
                    if (currentCube[a, b, c] != 0 && currentCube[a, b, c] < 7) //only adds the colors
                    {
                        intArrayCube.Add(currentCube[a, b, c]);
                    }
                }
            }
        }
      
        int[] transformedCube =
            { intArrayCube[35], intArrayCube[23], intArrayCube[11], intArrayCube[34], intArrayCube[22], intArrayCube[10], intArrayCube[33], intArrayCube[21], intArrayCube[9] ,intArrayCube[47], intArrayCube[46], intArrayCube[45], intArrayCube[50], intArrayCube[49], intArrayCube[48], intArrayCube[53], intArrayCube[52], intArrayCube[51],intArrayCube[36], intArrayCube[24], intArrayCube[12], intArrayCube[38], intArrayCube[26], intArrayCube[14], intArrayCube[40], intArrayCube[28], intArrayCube[16],intArrayCube[0], intArrayCube[1], intArrayCube[2], intArrayCube[3], intArrayCube[4], intArrayCube[5], intArrayCube[6], intArrayCube[7], intArrayCube[8],intArrayCube[13], intArrayCube[25], intArrayCube[37], intArrayCube[15], intArrayCube[27], intArrayCube[39], intArrayCube[17], intArrayCube[29], intArrayCube[41]
            ,intArrayCube[42], intArrayCube[30], intArrayCube[18], intArrayCube[43], intArrayCube[31], intArrayCube[19], intArrayCube[44], intArrayCube[32], intArrayCube[20]};
        string cubeToReturn = "";
        foreach (int i in transformedCube)
        {
            switch (i)
            {
                case 1:// white = 1
                    cubeToReturn += "W";
                    break;
                case 2: // blue = 2
                    cubeToReturn += "B";
                    break;
                case 3:// green = 3
                    cubeToReturn += "G";
                    break;
                case 4:// yellow = 4
                    cubeToReturn += "Y";
                    break;
                case 5://orange = 5
                    cubeToReturn += "O";
                    break;
                case 6://red = 6
                    cubeToReturn += "R";
                    break;
                case -1: // this is used if this part is supposed to not matter in the current solve state/  is masked.
                    cubeToReturn += "X"; 
                    break;

            }
           
        }
        return cubeToReturn;
    }
    /// <summary>
    /// This method acts as the main Solve method using the IDDFS technique to do so. It then calls on BackTrace when it finds a common string key within the other dictionary.
    /// </summary>
    /// <param name="ScrambledCube"> The scrambled cube in string form.</param>
    /// <param name="solvedCube"> The solved cube in string form.</param>
    /// <returns>The string that solves the current stage of solving.</returns>
    public static string Solving(string ScrambledCube, string solvedCube)
    {

        
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This method is not able to be put in seperate methods as it would have to create the method instance about 26^10 times per solve which increases solving time a lot. Sorry D:
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (ScrambledCube == solvedCube)
        {
            return "";
        }

        Queue<string> QueueSolved = new();
        Queue<string> QueueScrambled = new();
        List<string> possibleMovements = new() { "F", "U", "L", "R", "D", "B", "FF", "UU", "LL", "RR", "DD", "BB", "FFF", "UUU", "LLL", "RRR", "DDD", "BBB" };
        Dictionary<string, string> FromSolved = new();
        Dictionary<string, string> FromScramble = new();
     
   
        QueueSolved.Enqueue(solvedCube); // These Queues are used in order to give equal space to all cubes when solving also known as IDDFS
        QueueScrambled.Enqueue(ScrambledCube);

        bool FoundSolve = false;
        string SolveString = "";
        while (FoundSolve == false)
        {
            string oldCube = QueueSolved.Dequeue();
            foreach (string a in possibleMovements) // Goes through every possible movement after every dequeue
            {
                string newerCube = ApplyAlgorithm(oldCube, a); // Puts executed algorithm cube into newerCube
                if (FromScramble.ContainsKey(newerCube)) // If this goes to True the cube is found.
                {
                    FromSolved.Add(newerCube, oldCube);
                    SolveString = BackTraceSolve(FromSolved, FromScramble, newerCube, ScrambledCube, solvedCube,possibleMovements);
                    FoundSolve = true;
                }
                else if (FromSolved.TryAdd(newerCube, oldCube)) // it tries to add itself and if it is succesfull (if the key wasnt already in it it will also add itself back into queue
                {
                    QueueSolved.Enqueue(newerCube);
                }
            }

            if (FoundSolve == true) // This prevents errors in the BackTrace method if it found the cube at the first part.
                break;
            oldCube = QueueScrambled.Dequeue();
            foreach (string a in possibleMovements)
            {
                string newerCube = ApplyAlgorithm(oldCube, a);
                if (FromSolved.ContainsKey(newerCube))
                {
                    try
                    {
                        FromScramble.Add(newerCube, oldCube);
                    }
                    catch { }

                    SolveString = BackTraceSolve(FromSolved, FromScramble, newerCube, ScrambledCube, solvedCube,possibleMovements);
                  


                    FoundSolve = true;
                }
                else if (FromScramble.TryAdd(newerCube, oldCube))
                {
                    try
                    {
                        QueueScrambled.Enqueue(newerCube);
                    }
                    catch
                    {
                    }
                }
            }
        }
        return SolveString;
    }

    /// <summary>
    /// This method uses dictionaries in order to track back what the precise solve is and then puts it toghether
    /// </summary>
    /// <param name="FromSolvedState"> This is the dictionary that contains all the key pairs of the Solve->Lastusedcube</param>
    /// <param name="FromScrambledState"> This is the dictionary that went from Scrambled-> LastUsedcube</param>
    /// <param name="LastUsedCube"> This is the cube that was found in the other database</param>
    /// <param name="CubeGoal"> The scrambled cube</param>
    /// <param name="SolvedCube"> The solved cube in string form</param>
    /// <returns> A string containing the solve of the current solve state.</returns>
    public static string BackTraceSolve(Dictionary<string, string> FromSolvedState, Dictionary<string, string> FromScrambledState, string LastUsedCube, string CubeGoal, string SolvedCube,List<string> PossibleMoves)
    {
        List<string> ControlList = new(); // This list is used to detect if it is at the solved cube / scrambled to see if it should stop or not
        string NewerCube = LastUsedCube;
        string OlderCube = LastUsedCube;
        string StageOneSolveString = ""; // these strings are used by the program to build up the solution
        string StageTwoSolveString = "";

      
        ControlList.Add(LastUsedCube); //Puts itself in the list of cubes that it found already

        while (ControlList.Contains(CubeGoal) == false) // This part goes on until it reaches the scrambled cube state
        {
            NewerCube = FromScrambledState[OlderCube]; // This line uses the key to find the cube that it came from in order to find the right move to put into the solvestring

            foreach (var v in PossibleMoves)
            {

                if (ApplyAlgorithm(NewerCube, v) == OlderCube)
                {
                    StageOneSolveString = v + StageOneSolveString; // It stops here if it found its own path back to the older(later in branch) cube and then adds it to the string
                    break;
                }
            }

            ControlList.Add(NewerCube);
            OlderCube = NewerCube;
        }
        NewerCube = LastUsedCube;
        OlderCube = LastUsedCube;
        ControlList = new();
        ControlList.Add(LastUsedCube);
        NewerCube = FromSolvedState[LastUsedCube]; 
        while (ControlList.Contains(SolvedCube) == false)
        {
            NewerCube = FromSolvedState[OlderCube];


            foreach (string d in PossibleMoves)
            {

                if (ApplyAlgorithm(NewerCube, d) == OlderCube)
                {
                    StageTwoSolveString = StageTwoSolveString + d + d + d; // It has to add it 3x because it has to be reversed from the order it started in (this later gets shortened by the frontend when displayed.

                    break;
                }
            }
            ControlList.Add(NewerCube);
            OlderCube = NewerCube;
        }
        return StageOneSolveString + StageTwoSolveString;
    }


    /// <summary>
    /// This following method (and all the mentioned too small to comment on methods apply the algorithm that is provided by the user within the string that is inputted.
    /// </summary>
    /// <param name="currentCube"> the current string format cube </param>
    /// <param name="AlgorithmtoApply">This contains all the UFLRBD perms that need to be executed onto the cube</param>
    /// <returns>The string of the cube after all the perms have been done.</returns>
    public static string ApplyAlgorithm(string currentCube, string AlgorithmtoApply)
    {
        string NewCube = currentCube;
        char[] MoveList = AlgorithmtoApply.ToCharArray();
        foreach (char Move in MoveList)
        {



            switch (Move)
            {
                case 'F':
                    NewCube = F(NewCube);
                    break;
                case 'U':
                    NewCube = U(NewCube);
                    break;
                case 'L':
                    NewCube = L(NewCube);
                    break;
                case 'R':
                    NewCube = R(NewCube);
                    break;
                case 'B':
                    NewCube = B(NewCube);
                    break;
                case 'D':
                    NewCube = D(NewCube);
                    break;
                default:
                    return NewCube;


            }

        }
        return NewCube;
    }
     // The following methods are too small to each have seperate comments but they all return a series of manipulated parts of themselfs that satisfy the turn that had to be made by the ApplyAlgorithm.
    public static string R(string cube)
    {

        return cube.Substring(29, 1) + cube.Substring(32, 1) + cube.Substring(35, 1) + cube.Substring(3, 6) + cube.Substring(2, 1) + cube.Substring(10, 2) + cube.Substring(1, 1) + cube.Substring(13, 2) + cube.Substring(0, 1) + cube.Substring(16, 2) + cube.Substring(18, 9) + cube.Substring(27, 2) + cube.Substring(53, 1) + cube.Substring(30, 2) + cube.Substring(52, 1) + cube.Substring(33, 2) + cube.Substring(51, 1) + cube.Substring(42, 1) + cube.Substring(39, 1) + cube.Substring(36, 1) + cube.Substring(43, 1) + cube.Substring(40, 1) + cube.Substring(37, 1) + cube.Substring(44, 1) + cube.Substring(41, 1) + cube.Substring(38, 1) + cube.Substring(45, 6) + cube.Substring(9, 1) + cube.Substring(12, 1) + cube.Substring(15, 1)
            ;



    }
    public static string L(string cube)
    {

        return cube.Substring(0, 6) + cube.Substring(17, 1) + cube.Substring(14, 1) + cube.Substring(11, 1) + cube.Substring(9, 2) + cube.Substring(45, 1) + cube.Substring(12, 2) + cube.Substring(46, 1) + cube.Substring(15, 2) + cube.Substring(47, 1) + cube.Substring(24, 1) + cube.Substring(21, 1) + cube.Substring(18, 1) + cube.Substring(25, 1) + cube.Substring(22, 1) + cube.Substring(19, 1) + cube.Substring(26, 1) + cube.Substring(23, 1) + cube.Substring(20, 1) + cube.Substring(6, 1) + cube.Substring(28, 2) + cube.Substring(7, 1) + cube.Substring(31, 2) + cube.Substring(8, 1) + cube.Substring(34, 2) + cube.Substring(36, 9) + cube.Substring(33, 1) + cube.Substring(30, 1) + cube.Substring(27, 1) + cube.Substring(48, 6)
            ;
    }

    public static string U(string cube)
    {

        return cube.Substring(6, 1) + cube.Substring(3, 1) + cube.Substring(0, 1) + cube.Substring(7, 1) + cube.Substring(4, 1) + cube.Substring(1, 1) + cube.Substring(8, 1) + cube.Substring(5, 1) + cube.Substring(2, 1)
            + cube.Substring(18, 3) + cube.Substring(12, 6)
            + cube.Substring(27, 3) + cube.Substring(21, 6)
            + cube.Substring(36, 3) + cube.Substring(30, 6)
            + cube.Substring(9, 3) + cube.Substring(39, 6)
            + cube.Substring(45, 9)
            ;



    }
   
    public static string B(string cube)
    {


        return cube.Substring(44, 1) + cube.Substring(1, 2) + cube.Substring(41, 1) + cube.Substring(4, 2) +
               cube.Substring(38, 1) + cube.Substring(7, 2)
               + cube.Substring(15, 1) + cube.Substring(12, 1) + cube.Substring(9, 1) + cube.Substring(16, 1) +
               cube.Substring(13, 1) + cube.Substring(10, 1) + cube.Substring(17, 1) + cube.Substring(14, 1) +
               cube.Substring(11, 1)
               + cube.Substring(0, 1) + cube.Substring(19, 2) + cube.Substring(3, 1) + cube.Substring(22, 2) +
               cube.Substring(6, 1) + cube.Substring(25, 2)
               + cube.Substring(27, 9)
               + cube.Substring(36, 2) + cube.Substring(51, 1) + cube.Substring(39, 2) + cube.Substring(48, 1) +
               cube.Substring(42, 2) + cube.Substring(45, 1)
               + cube.Substring(18, 1) + cube.Substring(46, 2) + cube.Substring(21, 1) + cube.Substring(49, 2) +
               cube.Substring(24, 1) + cube.Substring(52, 2);

    }

    public static string F(string cube)
    {
        return cube.Substring(0, 2) + cube.Substring(20, 1) + cube.Substring(3, 2) + cube.Substring(23, 1) + cube.Substring(6, 2) + cube.Substring(26, 1)
            + cube.Substring(9, 9)
            + cube.Substring(18, 2) + cube.Substring(47, 1) + cube.Substring(21, 2) + cube.Substring(50, 1) + cube.Substring(24, 2) + cube.Substring(53, 1)
            + cube.Substring(33, 1) + cube.Substring(30, 1) + cube.Substring(27, 1) + cube.Substring(34, 1) + cube.Substring(31, 1) + cube.Substring(28, 1) + cube.Substring(35, 1) + cube.Substring(32, 1) + cube.Substring(29, 1)
            + cube.Substring(8, 1) + cube.Substring(37, 2) + cube.Substring(5, 1) + cube.Substring(40, 2) + cube.Substring(2, 1) + cube.Substring(43, 2)
            + cube.Substring(45, 2) + cube.Substring(42, 1) + cube.Substring(48, 2) + cube.Substring(39, 1) + cube.Substring(51, 2) + cube.Substring(36, 1)
            ;

    }
  
    public static string D(string cube)
    {

        return cube.Substring(0, 9)
               + cube.Substring(9, 6) + cube.Substring(42, 3)
               + cube.Substring(18, 6) + cube.Substring(15, 3)
               + cube.Substring(27, 6) + cube.Substring(24, 3)
               + cube.Substring(36, 6) + cube.Substring(33, 3)
               + cube.Substring(51, 1) + cube.Substring(48, 1) + cube.Substring(45, 1) + cube.Substring(52, 1) + cube.Substring(49, 1) + cube.Substring(46, 1) + cube.Substring(53, 1) + cube.Substring(50, 1) + cube.Substring(47, 1);
        ;



    }
}   