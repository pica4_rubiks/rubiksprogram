﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Windows.Media;

namespace rubikscube
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        //Many of the properties below are used in XAML bindings.

        //false is automatic
        private bool solveModeManual = false;
        public bool SolveModeManual { 
            get { return solveModeManual; }
            set {
                solveModeManual = value;
                OnPropertyChanged();
                OnPropertyChanged("SolveModeAutomatic");
            }
        }
        public bool SolveModeAutomatic { get { return !solveModeManual; } }
        public bool TimerEnabled { 
            get 
            {
                return Timer != null && Timer.Enabled;
            } 
        }
        public bool TimerDisabled
        {
            get { return !Timer.Enabled; }
        }
        public bool UndoRedoEnabled
        {
            get { return TimerDisabled || Timer.CheatMode; }
        }
        public bool SolvePreviousStepEnabled
        {
            get
            {
                return SolvePermsDone.Count > 0;
            } 
        }
        private bool resetToSolved;
        public bool ResetToSolved
        {
            get
            {
                return resetToSolved;
            }
            set
            {
                if (CubeOperations.CheckIfSolved(resetCube))
                    value = true;

                if (TimerEnabled)
                    value = false;

                resetToSolved = value;
                OnPropertyChanged("ResetText");
            }
        }

        public string ResetText
        {
            get
            {
                return resetToSolved ? "Complete Reset" : "Reset Cube";
            }
        }

        private List<string> solvePermsDone = new List<string>();
        private List<string> SolvePermsDone
        {
            get
            {
                return solvePermsDone;
            }
            set
            {
                OnPropertyChanged("SolvePreviousStepEnabled");
                solvePermsDone = value;
            }
        }

        private readonly Timer Timer;
        private DispatcherTimer SolvePlayPauseTimer = new DispatcherTimer(DispatcherPriority.Send);
        private HelpPopup HelpPopup;

        private CubeGraphics CubeG;
        private int amountOfRots = 1;

        public Cube thisCube = new();
        public Cube resetCube;

        private Random randomInt = new Random();

        private bool ShiftPressed = false;
        private bool CtrlPressed = false;

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            SetResetCube();

            Timer = new Timer(this);
            HelpPopup = new HelpPopup((Grid)FindName("main_grid"));

            SolvePlayPauseTimer.Tick += new EventHandler(SolveNextStep);
            SolvePlayPauseTimer.Interval = TimeSpan.FromSeconds(1.2);

            CubeG = new CubeGraphics((ModelVisual3D)FindName("rubiks_cube"), thisCube.GetGraphicsDict());
            UpdateCube();
        }

        private void SaveCube(object o, EventArgs e)
        {
            if (TimerEnabled)
                return;

            CubeOperations.SaveCube(thisCube);
        }

        private void LoadCube(object o, EventArgs e)
        {
            if (TimerEnabled)
                return;

            Cube? c = CubeOperations.LoadCube();

            if (c == null)
                return;

            thisCube = c;
            UpdateCube(setResetCube: true);
        }

        private void ManualImport(object o, EventArgs e)
        {
            ManuallyImportCubePopup p = new ManuallyImportCubePopup();
            if (!p.ShowDialog() ?? false)
                return;

            UpdateCube(Cube.Parse(p.State), true);
        }

        private void ChangeSolveMode(object o, EventArgs e)
        {
            if (SolveModeManual)
            {
                if (TimerEnabled)
                {
                    MessageBoxResult r = MessageBox.Show("Are you sure you want to switch modes?\n\nThis will stop the timer.", "Switch modes", MessageBoxButton.OKCancel);
                    if (r == MessageBoxResult.Cancel)
                        return;
                }

                Timer.Stop();
                UpdatePerms();
            } 

            ((StackPanel)FindName("timer_buttons")).Visibility = SolveModeManual ? Visibility.Collapsed : Visibility.Visible;
            ((Button)FindName("enablecheatmode_button")).Visibility = SolveModeManual ? Visibility.Collapsed : Visibility.Visible;

            ((StackPanel)FindName("solve_buttons")).Visibility = SolveModeManual ? Visibility.Visible : Visibility.Collapsed;
            ((ScrollViewer)FindName("steps_done")).Visibility = SolveModeManual ? Visibility.Visible : Visibility.Collapsed;

            ((MenuItem)FindName("solve_mode_button")).Header = SolveModeManual ? "Solve Mode: Automatic" : "Solve Mode: Manual";
            ((TextBlock)FindName("steps_done_text")).Text = SolveModeManual ? "Solution:" : "Moves done:";

            SolveModeManual = !SolveModeManual;

            if (!solveModeManual)
                UpdatePerms();
        }

        public void ChangeStartOnMove(object o, EventArgs e)
        {
            bool isChecked = ((CheckBox)o).IsChecked ?? false;
            Timer.StartOnMove = isChecked;
            ((Button)FindName("timer_button")).IsEnabled = !isChecked;
        }

        private void StartStopTimer(object o, EventArgs e)
        {
            if (Timer.Stop())
                return;

            Timer.Start();
        }

        private void ToggleCheatMode(object o, EventArgs e)
        {
            Timer.ToggleCheatMode((Button)o);
        }

        private void UndoMove(object o, EventArgs e)
        {
            if (TimerEnabled && !Timer.CheatMode || Timer.CheatMode && thisCube.PermsTextList.Count <= resetCube.PermsTextList.Count)
                return;

            thisCube.UndoMove(this);
        }
 
        private void RedoMove(object o, EventArgs e)
        {
            if (TimerEnabled && !Timer.CheatMode)
                return;

            thisCube.RedoMove(this);
        }

        private void ResetCube(object o, EventArgs e)
        {
            if (ResetToSolved)
                UpdateCube(new Cube());
            else
            {
                UpdateCube(resetCube);
                ResetToSolved = true;
            }
        }

        public void SetResetCube()
        {
            resetCube = thisCube.Clone();
            ResetToSolved = true;
        }

        private void ChangeFaceNotation(object o, EventArgs e)
        {
            CubeG.UpdateModel(CubeG.Cube, !CubeG.DirectionTextEnabled);
            CubeG.DrawCube();
        }

        private void RotateButton_Click(object o, EventArgs e)
        {
                RotateFace(((Button)o).Name[0], amountOfRots);
        }

        private void RotateMenu_Click(object o, EventArgs e)
        {
            RotateFace(((MenuItem)o).Tag.ToString()[0], amountOfRots);
        }

        /// <summary>
        /// Rotates one face of the cube.
        /// </summary>
        /// <param name="f">The face to rotate.</param>
        /// <param name="rots">The amount to rotate the face.</param>
        /// <param name="clearRedoStack">Whether to clear the redostack or not.</param>
        public void RotateFace(char f, int rots, bool clearRedoStack = true)
        {
            if (!TimerEnabled && Timer.StartOnMove)
            {
                if (!Timer.Start())
                    return;
            }

            thisCube.Values = thisCube.PermCube(thisCube.Values, f, rots);

            if (clearRedoStack)
            {
                thisCube.RedoStack.Clear();
                SolvePermsDone.Clear();
                OnPropertyChanged("SolvePreviousStepEnabled");
            }

            UpdateCube();

            if (TimerEnabled && CubeOperations.CheckIfSolved(thisCube))
                Timer.CubeSolvedManually();
        }

        /// <summary>
        /// Updates the cube on screen.
        /// </summary>
        /// <param name="newCube">A new to update to, if null, thisCube is used.</param>
        /// <param name="setResetCube">Whether to also set the reset cube or not.</param>
        private void UpdateCube(Cube? newCube = null, bool setResetCube = false)
        {
            if (newCube != null)
                thisCube = newCube.Clone();

            if (setResetCube)
                SetResetCube();
            else
                ResetToSolved = false;

            UpdatePerms(TimerEnabled ? (newCube ?? resetCube).PermsTextList.Count : 0);

            CubeG.UpdateModel(thisCube.GetGraphicsDict(), CubeG.DirectionTextEnabled);
            CubeG.DrawCube();
        }

        private void RotateCW(object o, EventArgs e)
        {
            amountOfRots = 1;
        }
        private void RotateCCW(object o, EventArgs e)
        {
            amountOfRots = 3;
        }
        private void RotateTwice(object o, EventArgs e)
        {
            amountOfRots = 2;
        }

        private void GenerateRandomSeed(object o, EventArgs e)
        {
            seedInput.Text = randomInt.Next().ToString();
        }
 
        private void ScrambleCube(object sender, RoutedEventArgs e)
        {
            if (TimerEnabled)
                return;

            thisCube = CubeOperations.getScrambledCube(seedInput.Text);
            UpdateCube(setResetCube: true);
        }

        private void SolveCube(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            thisCube.Solution = CubeOperations.OptimizePermsList(Solver.TheSolver(thisCube));

            UpdatePerms();

            Mouse.OverrideCursor = null;
        }

        private void SolvePreviousStep(object sender, RoutedEventArgs e)
        {
            if (SolvePlayPauseTimer.IsEnabled)
                SolvePlayPause(FindName("solve_playpause_button"), null);

            if (SolvePermsDone.Count == 0)
                return;

            thisCube.Values = thisCube.PermCube(thisCube.Values, SolvePermsDone[0][0], 4 - SolvePermsDone[0].Length, updateSolutionOptimality: false);
            SolvePermsDone.RemoveAt(0);
            OnPropertyChanged("SolvePreviousStepEnabled");

            UpdateCube();
        }

        private void SolveNextStep(object? sender, EventArgs e)
        {
            if ((sender.GetType() != typeof(DispatcherTimer) || thisCube.Solution.Count == 0) && SolvePlayPauseTimer.IsEnabled)
                SolvePlayPause(FindName("solve_playpause_button"), null);

            if (thisCube.Solution.Count == 0)
                return;

            string move = thisCube.Solution[0];
            thisCube.Values = thisCube.PermCube(thisCube.Values, move[0], move.Length, updateSolution: false, updateSolutionOptimality: false);

            solvePermsDone.Insert(0, move);
            OnPropertyChanged("SolvePreviousStepEnabled");

            if (thisCube.Solution.Count > 0)
                thisCube.Solution.RemoveAt(0); 

            UpdateCube();
        }

        private void SolvePlayPause(object sender, RoutedEventArgs e)
        {
            Button? b = sender as Button;
            string bContent;

            if (SolvePlayPauseTimer.IsEnabled)
            {
                SolvePlayPauseTimer.Stop();
                bContent = "Play";
            }
            else
            {
                SolvePlayPauseTimer.Start();
                bContent = "Pause";
            }

            if (b != null)
                b.Content = bContent;
        }

        /// <summary>
        /// Updates the steps done/solution text.
        /// </summary>
        /// <param name="startIndex">How many permutations to skip before starting.</param>
        /// <exception cref="ArgumentException">Startindex cannot be >0 in automatic mode.</exception>
        public void UpdatePerms(int startIndex = 0)
        {
            List<string> perms;

            if (SolveModeAutomatic)
            {
                Button solveButton = (Button)FindName("solve_button");
                Button solvePlayPauseButton = (Button)FindName("solve_playpause_button");
                Button nextButton = (Button)FindName("solve_next_step_button");

                if (!thisCube.SolutionKnown)
                {
                    solveButton.Content = "Solve Cube";
                    solveButton.IsEnabled = true;
                    solvePlayPauseButton.IsEnabled = false;
                    nextButton.IsEnabled = false;

                    permsText.Text = "Solution not available. Use the \"Solve\" button to calculate a solution.";
                    return;
                }
                else if (CubeOperations.CheckIfSolved(thisCube))
                {
                    solveButton.Content = "Solve Cube";
                    solveButton.IsEnabled = false;
                    solvePlayPauseButton.IsEnabled = false;
                    nextButton.IsEnabled = false;

                    permsText.Text = "The cube is solved.";
                    return;
                }
                else if (thisCube.SolutionOptimal)
                {
                    solveButton.Content = "Recalculate Solution";
                    solveButton.IsEnabled = false;
                    solvePlayPauseButton.IsEnabled = true;
                    nextButton.IsEnabled = true;
                }
                else
                {
                    solveButton.Content = "Recalculate Solution";
                    solveButton.IsEnabled = true;
                    solvePlayPauseButton.IsEnabled = true;
                    nextButton.IsEnabled = true;
                }

                if (startIndex > 0)
                    throw new ArgumentException();

                perms = thisCube.Solution;
            }
            else
                perms = thisCube.PermsTextList;

            List<string> formattedPerms = CubeOperations.FormatPerms(perms.Skip(startIndex).ToList());

            permsText.Text = string.Join(",", formattedPerms); //set the text on screen to formatted list
        }

        private void OpenPopup(object sender, EventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            HelpPopup.IsOpen = true;

            switch(mi.Header)
            {
                case "_Different Modes":
                    HelpPopup.DifferentModes();
                    break;
                case "_Reset button and face notation":
                    HelpPopup.ResetButtonFaceNotation();
                    break;
                case "_Viewport Controls":
                    HelpPopup.ViewPort();
                    break;
                case "_Cube Rotations":
                    HelpPopup.CubeRots();
                    break;
                case "_Seed-based Scrambler":
                    HelpPopup.SeedScramble();
                    break;
                case "_Im- and Exporting Cubes":
                    HelpPopup.ImportExport();
                    break;
                case "_Manually importing real-life Cube":
                    HelpPopup.ManualEnter();
                    break;
                case "_Solving the Cube":
                    HelpPopup.SolveFeature();
                    break;
            }

            HelpPopup.IsOpen = true;
        }

        /// <summary>
        /// Handles keypresses.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            char? key = null;
            int rots = ShiftPressed ? 3 : 1;

            if (e.Key == Key.F) key = 'F';
            else if (e.Key == Key.B) key = 'B';
            else if (e.Key == Key.L) key = 'L';
            else if (e.Key == Key.R) key = 'R';
            else if (e.Key == Key.X) key = 'x';
            else if (e.Key == Key.Y) key = 'y';
            else if (e.Key == Key.Z && !CtrlPressed) key = 'z';
            else if (e.Key == Key.U) key = 'U';
            else if (e.Key == Key.D) key = 'D';
            else if (e.Key == Key.M) key = 'M';
            else if (e.Key == Key.E) key = 'E';
            else if (e.Key == Key.S) key = 'S';
            else if (e.Key == Key.LeftShift) ShiftPressed = true;
            else if (e.Key == Key.LeftCtrl) CtrlPressed = true;
            else if (e.Key == Key.Escape) HelpPopup.IsOpen = false;
            if (key != null)
                RotateFace((char)key, rots);

            if (e.Key == Key.Z && CtrlPressed && ShiftPressed)
                RedoMove(null, null);
            else if (e.Key == Key.Z && CtrlPressed)
                UndoMove(null, null);
        }

        void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift) ShiftPressed = false;
            else if (e.Key == Key.LeftCtrl) CtrlPressed = false;
        }
    }
}
