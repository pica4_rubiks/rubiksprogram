﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text.Json.Nodes;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;

namespace rubikscube
{
    /// <summary>
    /// Interaction logic for ManuallyImportCubePopup.xaml
    /// </summary>
    public partial class ManuallyImportCubePopup : Window, INotifyPropertyChanged
    {
        // Colors from CubeGraphics cannot be used because these are a different object.
        public static SolidColorBrush White { get { return new SolidColorBrush(Colors.White); } }
        public static SolidColorBrush Blue { get { return new SolidColorBrush(Color.FromRgb(0, 128, 255)); } }
        public static SolidColorBrush Green { get { return new SolidColorBrush(Color.FromRgb(0, 255, 0)); } }
        public static SolidColorBrush Yellow { get { return new SolidColorBrush(Color.FromRgb(255, 255, 0)); } }
        public static SolidColorBrush Orange { get { return new SolidColorBrush(Color.FromRgb(255, 128, 64)); } }
        public static SolidColorBrush Red { get { return new SolidColorBrush(Color.FromRgb(255, 0, 0)); } }

        public Dictionary<string, int[]> State;
        public bool Valid { get { return valid; } }
        public static Dictionary<string, int[]> DefaultState
        {
            get
            {
                return JsonConvert.DeserializeObject<Dictionary<string, int[]>>(JsonConvert.SerializeObject(CubeGraphics.DefaultCube));
            }
        }

        private SolidColorBrush SelectedBrush;
        private bool valid = true;
        private Dictionary<Color, int> colors = new Dictionary<Color, int>()
        {
            { White.Color, 1 },
            { Blue.Color, 2 },
            { Green.Color, 3},
            { Yellow.Color, 4},
            { Orange.Color, 5},
            { Red.Color, 6 }
        };

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ManuallyImportCubePopup()
        {
            InitializeComponent();
            DataContext = this;

            State = DefaultState;
            SelectedBrush = Red;

            UpdateIssues();
        }

        /// <summary>
        /// Changes the selected color.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void ChangeSelectedColor(object o, EventArgs e)
        {
            Button b = (Button)o;
            SelectedBrush = (SolidColorBrush)b.Background;

            Ellipse indicator = (Ellipse)FindName("indicator_ellipse");
            Grid.SetRow(indicator, Grid.GetRow(b));
            Grid.SetColumn(indicator, Grid.GetColumn(b));
        }

        /// <summary>
        /// Changes the color of a square.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void ChangeColor(object o, EventArgs e)
        {
            Dictionary<(int col, int row), string> sides = new Dictionary<(int, int), string>()
            {
                { (0, 1), "left" },
                { (2, 1), "right" },
                { (1, 0), "top" },
                { (1, 2), "bottom" },
                { (1, 1), "front"},
                { (3, 1), "back" }
            };

            Dictionary<(int col, int row), int> faces = new Dictionary<(int, int), int>()
            {
                { (0, 0), 0 },
                { (1, 0), 1 },
                { (2, 0), 2 },
                { (0, 1), 3 },
                { (1, 1), 4 },
                { (2, 1), 5 },
                { (0, 2), 6 },
                { (1, 2), 7 },
                { (2, 2), 8 },
            };

            Label l = (Label)o;

            Border b = (Border)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(l));
            string key = sides[(Grid.GetColumn(b), Grid.GetRow(b))];
            int index = faces[(Grid.GetColumn(l), Grid.GetRow(l))];

            State[key][index] = colors[SelectedBrush.Color];
            UpdateIssues();
            l.Background = SelectedBrush;
        }

        /// <summary>
        /// Helper method to ColorAmounts(). Counts how many squares of every color there are.
        /// </summary>
        /// <returns>The amount of squares of every color.</returns>
        private int[] CountSquareColors()
        {
            int[] colorCounts = new int[6];

            foreach (KeyValuePair<string, int[]> face in State)
            {
                for (int i = 0; i < face.Value.Length; i++)
                {
                    colorCounts[face.Value[i] - 1]++;
                }
            }

            return colorCounts;
        }

        /// <summary>
        /// Helper method to UpdateIssues(). Counts the amount squares for every color and converts it to text.
        /// </summary>
        /// <returns>A string containing the colors for which the amount is not right.</returns>
        private string ColorAmounts()
        {
            string text = "";
            int[] colorCounts = CountSquareColors();

            for (int i = 0; i < colorCounts.Length; i++)
            {
                if (colorCounts[i] == 9)
                    continue;

                if (colorCounts[i] < 9)
                    text += "You do not have enough ";
                else
                    text += "You have too many ";

                switch (i)
                {
                    case 0: text += "white "; break;
                    case 1: text += "blue "; break;
                    case 2: text += "green "; break;
                    case 3: text += "yellow "; break;
                    case 4: text += "orange "; break;
                    case 5: text += "red "; break;
                }

                text += $"squares. ({colorCounts[i]}/9)\n";
            }

            return text;
        }

        /// <summary>
        /// Helper method to UpdateIssues(). Checks if every center has a different color.
        /// </summary>
        /// <returns>A bool that is true if every center has a different color</returns>
        private bool CheckCenters()
        {
            List<int> colors = new List<int>();

            foreach (KeyValuePair<string, int[]> face in State)
            {
                if (colors.Contains(face.Value[4]))
                    return false;

                colors.Add(face.Value[4]);
            }

            return true;
        }

        /// <summary>
        /// Helper method to UpdateIssues(). Checks if the colors pairs on opposite sides of the cube are correct.
        /// </summary>
        /// <returns>A bool that is true if the colors pairs on opposite sides of the cube are correct.</returns>
        private bool CheckAxis()
        {
            Dictionary<int, int> colorPairs = new Dictionary<int, int>()
            {
                { 1, 4 },
                { 2, 3 },
                { 3, 2 },
                { 4, 1 },
                { 5, 6 },
                { 6, 5 },
            };

            Dictionary<string, string> axis = new Dictionary<string, string>()
            {
                { "top", "bottom" },
                { "left", "right" },
                { "front", "back" },
            };

            foreach (KeyValuePair<string, string> a in axis)
            {
                int color = State[a.Key][4];
                int oppositeColor = State[a.Value][4];

                if (colorPairs[color] != oppositeColor)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Does one clockwise corner twist.
        /// </summary>
        /// <param name="corner">The corner you want to twist</param>
        /// <param name="side">The side you want to know the new position of.</param>
        /// <returns>The side the old side moved to.</returns>
        /// <exception cref="ArgumentException">Corner or side is not valid.</exception>
        private string CWCornerTwist(string[] corner, string side)
        {
            switch (side)
            {
                case "front":
                    if (corner.Contains("top"))
                        return corner.Contains("left") ? "left" : "top";
                    else if (corner.Contains("bottom"))
                        return corner.Contains("left") ? "bottom" : "right";
                    break;
                case "back":
                    if (corner.Contains("top"))
                        return corner.Contains("left") ? "top" : "right";
                    else if (corner.Contains("bottom"))
                        return corner.Contains("left") ? "left" : "bottom";
                    break;
                case "left":
                    if (corner.Contains("top"))
                        return corner.Contains("front") ? "top" : "back";
                    else if (corner.Contains("bottom"))
                        return corner.Contains("front") ? "front" : "bottom";
                    break;
                case "right":
                    if (corner.Contains("top"))
                        return corner.Contains("front") ? "front" : "top";
                    else if (corner.Contains("bottom"))
                        return corner.Contains("front") ? "bottom" : "back";
                    break;
                case "top":
                    if (corner.Contains("front"))
                        return corner.Contains("left") ? "front" : "right";
                    else if (corner.Contains("back"))
                        return corner.Contains("left") ? "left" : "back";
                    break;
                case "bottom":
                    if (corner.Contains("front"))
                        return corner.Contains("left") ? "left" : "front";
                    else if (corner.Contains("back"))
                        return corner.Contains("left") ? "back" : "right";
                    break;
            }
            throw new ArgumentException("Invalid side or corner!");
        }

        /// <summary>
        /// Helper method to UpdateIssues(). Checks the corner parity of the cube.
        /// </summary>
        /// <returns>A bool that is true if the corner parity is valid.</returns>
        private bool CornerParity()
        {            
            List<Dictionary<string, int>> corners = new List<Dictionary<string, int>>()
            { 
                new() { { "front", 0 }, { "top", 6 }, { "left", 2 } },
                new() { { "front", 2 }, { "top", 8 }, { "right", 0 } },
                new() { { "front", 6 }, { "bottom", 0 }, { "left", 8 } },
                new() { { "front", 8 }, { "bottom", 2 }, { "right", 6 } },
                new() { { "back", 2 }, { "top", 0 }, { "left", 0 } },
                new() { { "back", 0 }, { "top", 2 }, { "right", 2 } },
                new() { { "back", 8 }, { "bottom", 6 }, { "left", 6 } },
                new() { { "back", 6 }, { "bottom", 8 }, { "right", 8 } }
            };

            List<Dictionary<string, int>> targetAxisColors = new List<Dictionary<string, int>>()
            {
                new() { { "top", State["top"][4] }, { "bottom", State["bottom"][4] } },
                new() { { "front", State["front"][4] }, { "back", State["back"][4] } },
                new() { { "left", State["left"][4] }, { "right", State["right"][4] } }
            };

            foreach (Dictionary<string, int> targetSidesColors in targetAxisColors)
            {
                int cornerTwists = 0;

                foreach (Dictionary<string, int> corner in corners)
                {
                    string[] cornerSides = corner.Keys.ToArray();
                    Dictionary<string, int> cornerSidesColors = new();
                    for (int i = 0; i < cornerSides.Length; i++)
                    {
                        cornerSidesColors.Add(cornerSides[i], State[cornerSides[i]][corner[cornerSides[i]]]);
                    }

                    KeyValuePair<string, int> currentSideColor;
                    try
                    {
                        currentSideColor = cornerSidesColors.First(x => targetSidesColors.Values.ToArray().Contains(x.Value));
                    }
                    catch (InvalidOperationException)
                    {
                        return false;
                    }

                    string currentSide = currentSideColor.Key;
                    while (!targetSidesColors.Keys.ToArray().Contains(currentSide))
                    {
                        cornerTwists++;
                        currentSide = CWCornerTwist(cornerSides, currentSide);
                    }
                }

                if (!(cornerTwists % 3 == 0))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// finds cubie based on its int value
        /// </summary>
        /// <param name="cubie">integer of cubie</param>
        /// <param name="cube">a 3d array of the cube to look for the cubie</param>
        /// <returns>tuple of cubie coordinates</returns>
        private Tuple<int, int, int> findCubie(int cubie, int[,,] cube)
        {
            for (int x = 1; x < cube.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < cube.GetLength(1) - 1; y++)
                {
                    for (int z = 1; z < cube.GetLength(2) - 1; z++)
                    {
                        if (cube[x, y, z] == cubie)
                        {
                            Tuple<int, int, int> position = Tuple.Create(x, y, z);
                            return position;
                        }
                    }
                }
            }

            return Tuple.Create(0, 0, 0);
        }

        /// <summary>
        /// the permutation parity function.  Checks wether or not the permutation (or; number of swaps) is even or not.
        /// </summary>
        /// <returns>boolean</returns>
        private bool PermParity()
        {
            List<int> cubiesToCheck = new List<int>() { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }; //list with corner values to check
            List<char> revertPerms = new List<char>();
            int[,,] inputCube = Cube.Parse(State).Values;
            int[,,] DefaultCube = Cube.DefaultCube();
            int rotateAxis = 0;
            
            while (!findCubie(11, inputCube).Equals(findCubie(11, DefaultCube))) //rotate redcubie to front
            {
                if (rotateAxis == 4)
                {
                    inputCube = Cube.SolvePerms(inputCube, 'y');
                    revertPerms.Add('y');
                }
                else
                {
                    inputCube = Cube.SolvePerms(inputCube, 'x');
                    revertPerms.Add('x');
                }

                rotateAxis++;
            }
            while (!findCubie(19, inputCube).Equals(findCubie(19, DefaultCube))) //rotate whitecubie to top
            {
                inputCube = Cube.SolvePerms(inputCube, 'z');
                revertPerms.Add('z');
            }
            int amountOfHops = 0;
            foreach (var cubie in cubiesToCheck)
            {
                //foreach value in cubiesToCheck, check pos in inputCube. If(goedePos)--> goto next value. If not, swap with goedePos, and check again
                while (!findCubie(cubie, DefaultCube).Equals(findCubie(cubie, inputCube)))
                {
                    Tuple<int, int, int> defaultPos = findCubie(cubie, DefaultCube);
                    Tuple<int, int, int> inputPos = findCubie(cubie, inputCube);
                    inputCube[inputPos.Item1, inputPos.Item2, inputPos.Item3] = inputCube[defaultPos.Item1, defaultPos.Item2, defaultPos.Item3];
                    inputCube[defaultPos.Item1, defaultPos.Item2, defaultPos.Item3] = cubie;
                    amountOfHops++;
                }
            }
            if (amountOfHops % 2 != 0)
            {
                return false;
            }
            
            revertPerms.Reverse();
            foreach (var perm in revertPerms)
            {
                inputCube = Cube.SolvePerms(inputCube, perm);
                inputCube = Cube.SolvePerms(inputCube, perm);
                inputCube = Cube.SolvePerms(inputCube, perm);
            }
            return true;
        }

        /// <summary>
        /// Updates the issues text.
        /// </summary>
        private void UpdateIssues()
        {
            string text = "";

            if (!CheckCenters())
                text += "There needs to be exactly one center of every color.\n";
            else if (!CheckAxis())
                text += "Invalid color pairs on one or more axis.\n";

            text += ColorAmounts();

            if (text == "")
            {
                if (!CornerParity())
                    text += "Invalid corner parity.\n";
                if (!PermParity())
                    text += "Invalid permutation parity.\n";
            }
            else
                valid = false;

            if (text == "")
            {
                text = "There are no issues.";
                valid = true;
            }
            else
                valid = false;

            OnPropertyChanged("Valid");

            TextBlock tb = (TextBlock)FindName("issues_textblock");
            tb.Text = text;
        }

        /// <summary>
        /// Closes the dialog with a positive result.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void Finish(object o, EventArgs e)
        {
            if (!Valid)
                return;

            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Asks the user if the dialog should really be closed if changes have been made.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void ConfirmClose(object o, CancelEventArgs e)
        {
            if (DialogResult == true)
                return;

            bool equal = true;
            foreach(KeyValuePair<string, int[]> face in State)
            {
                if (!face.Value.SequenceEqual(CubeGraphics.DefaultCube[face.Key]))
                {
                    equal = false;
                    break;
                }
            }
            if (equal)
                return;

            MessageBoxResult r = MessageBox.Show("Are you sure you want to close this window?\nYour changes will be lost.",
                "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (r == MessageBoxResult.No)
                e.Cancel = true;
        }
    }
}
