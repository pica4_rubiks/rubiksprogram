﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace rubikscube
{
    internal class HelpPopup : Popup
    {
        private TextBlock TB;
        private readonly Color BackgroundColor = Color.FromRgb(245, 245, 245);
        private readonly int Padding = 20;

        private Grid Parent;

        public HelpPopup(Grid parent)
        {
            IsOpen = false;
            Placement = PlacementMode.Center;
            StaysOpen = false;

            Border b = new Border();
            b.BorderThickness = new System.Windows.Thickness(1);
            Child = b;
            b.Cursor = Cursors.SizeAll;

            TB = new TextBlock() 
            {
                Background = new SolidColorBrush(BackgroundColor),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Padding = new Thickness(Padding, Padding - 10, Padding, Padding),
                TextWrapping = TextWrapping.Wrap
            };

            TextBlock closeButton = new TextBlock()
            {
                Background = new SolidColorBrush(BackgroundColor),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Top,
                Text = "Click anywhere else to close",
                TextAlignment = TextAlignment.Right,
                Padding = new Thickness(0, 10, 10, 0),
                FontWeight = FontWeights.Bold
            };

            StackPanel stackPanel = new()
            {
            };

            stackPanel.Children.Add(closeButton);
            stackPanel.Children.Add(TB);

            b.Child = stackPanel;
            Child = b;
            parent.Children.Add(this);
            Parent = parent;

            MouseDown += StartDragging;
            MouseMove += Drag;
            MouseUp += StopDragging;
        }

        private bool Dragging = false;
        private double MouseX;
        private double MouseY;
        private void StartDragging(object o, EventArgs e)
        {
            Dragging = true;
            Point mousePos = Mouse.GetPosition(Parent);
            MouseX = mousePos.X;
            MouseY = mousePos.Y;
        }

        private void Drag(object o, EventArgs e)
        {
            if (!Dragging)
                return;

            Point mousePos = Mouse.GetPosition(Parent);

            HorizontalOffset += mousePos.X - MouseX;
            VerticalOffset += mousePos.Y - MouseY;

            MouseX = mousePos.X;
            MouseY = mousePos.Y;
        }

        private void StopDragging(object o, EventArgs e)
        {
            Dragging = false;
        }

        public void DifferentModes()
        {
            TB.Text = "There are 2 different modes; the \"automatic\" mode, and the \"manual\" mode. \n \n " +
                "The automatic mode allows you to let the program generate a solution for the cube, and walk through that solution step-by-step. \n \n " +
                "The manual mode allows you to time yourself solving the cube, from a scrambled state. \n \n" +
                "To switch from \"automatic\" to \"manual\" and back, simply press the button in the top right corner named \"Solve Mode:\"";
        }

        public void ResetButtonFaceNotation()
        {
            TB.Text = "On the left side of the program, there is the left hand menu. " +
                      "At the top of this menu, there are the \"face notation\" checkbox, and the Reset cube button. \r \n" +
                      "The face notation button either enables or disables the black letters on the 3d cube. These are used to indicate which face is which, and can be en- or disabled using this checkbox. \r \n" +
                      "The reset button resets the cube to a checkpoint. Checkpoints are set when you scramble or import a cube.\n" +
                      "If you reset to a checkpoint, the reset button will change to \"Complete Reset\". Clicking it will now reset the cube to a fully solved state.";
        }
        public void ViewPort()
        {
            TB.Text = "The viewport is the \"camera\" of the program. You are able to manipulate the viewport in 2 ways; rotation and zoom. \n \n " +
                "For rotation; \n Use the arrow keys (↑, ↓, ←, →), or\r\n" +
                "Hold Left-Ctrl and press one of the face keys (F, B, L, R, U, D), or\r\n" +
                "Press one of the faces on the small cube in the bottom right corner\r\n" +
                "Hold RightMouseClick and drag your cursor. \n \n" +
                "Zooming in- and out of the cube can be done the following ways:\r\n" +
                "by pinching or spreading 2 fingers on your trackpad\r\n" +
                "by rotating the scroll wheel on your mouse";
        }
        public void CubeRots()
        {
            TB.Text = "Rotating the cube can be done in 2 ways; by using the lefthand menu, or the keyboard shortcuts.\r\n" +
                "In the lefthand menu, there are 2 parts. \n " +
                "The amount part has 3 radio buttons, where you can choose between 1 rotation (in the case of F that would be F), \n" +
                " 2 rotations (F2), or 3 rotations (F'). \n \n" +
                "After selecting the amount in the amount part, a button (Upward, Left, Front, Right, Downward, Back) in \n" +
                " the rotation part can be pressed to rotate the cube [amount] times on the [rotation] face. \n \n" +
                "The cube can also be rotated using the following keyboard shortcuts:\r\n" +
                "F does one front rotation\r\n" +
                "B: does one back rotation\r\n" +
                "L: does one left rotation\r\n" +
                "R: does one right rotation\r\n" +
                "U: does one up rotation\r\n" +
                "D: does one down rotation\r\n" +
                "M: does one rotation of the middle layer over the X axis\r\n" +
                "E: does one rotation of the middle layer over the X axis\r\n" +
                "S: does one rotation of the middle layer over the X axis\r\n" +
                "X: does one rotation of the full cube over the X axis\r\n" +
                "Y: does one rotation of the full cube over the Y axis\r\n" +
                "Z: does one rotation of the full cube over the Z axis\r\n" +
                "optionally, when holding down Right-Shift and pressing one of the buttons stated above, a counterclockwise rotation will be done instead of a clockwise rotation.\r\n" +
                "You can undo and redo moves using the buttons under the \"Action\" menu or by using Ctrl+Z or Ctrl+Shift+Z respectively.";
        }

        public void SeedScramble()
        {
            TB.Text = "In order to generate the same scrambled cube over and over again, you are able to enter a seed. \n" +
                "When entering the same seed, the same scramble will be generated. \n" +
                "Scrambles are always based on a \"solved\" cube.\r\n\r\n" +
                "The seed menu consists of 3 parts:\r\n" +
                "Seed input field (the textbox after \"seed:\"): here one can enter a seed.\r\n" +
                "Random seed button: this button generates a (pseudo-)random seed, and enters that seed into the seed input field.\r\n" +
                "Scramble button: scrambles the cube based on the seed in the seed input field.";
        }

        public void ImportExport()
        {
            TB.Text = "Using the \"Save\" button under \"File\", you can save your current cube to a file.\n" +
                "This allows you to save the current state for later or share it with a friend.\n\nTo import the cube," +
                " use the \"Open\" in the same menu and select the file.";
        }

        public void ManualEnter()
        {
            TB.Text = "In order to manually import a cube, click on \"Cube\" in the top-left corner.\r\n" +
                      "Then, select \"Manual Import\".\n" +
                      "Here, you are able to import a cube based on a cube in real life. When you have colored in your cube, click Import.\n" +
                      "There might be some text stating your cube is not valid, and the reason why. " +
                      "In that case, please double-check your virtual cube against the cube you are holding in your hand and make sure you did not make any mistakes.";
        }

        public void SolveFeature()
        {
            TB.Text = "When in the solver mode, in the bottom of the left-hand control menu, there is a text field titled \"Solution:\". When performing the perms in that field, " +
                "the cube will be solved.\nAlternatively, press the play/pause button to automatically solve the cube step-by-step. If you missed a step, you can use the Previous step and Next step buttons to go back or forward in the solution.\r\n\r\n" +
                "It might be the case that you have a long solution, or the solution might not be available after importing a cube.\n" +
                "In that case, press the Solve Cube/Recalculate solution button, to calculate a new solution to the cube.";
        }
    }
}
