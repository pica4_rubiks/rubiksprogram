﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rubikscube
{
    using System.Windows.Media;
    using System.Windows.Media.Media3D;
    using HelixToolkit.Wpf;
    using System.Drawing;
    using Color = System.Drawing.Color;
    using System.IO;
    using System.Drawing.Drawing2D;
    using System.Diagnostics;

    public class CubeGraphics
    {
        public static readonly Dictionary<string, int[]> DefaultCube = new()
        {
            { "top",  new int[9]{1, 1, 1, 1, 1, 1, 1, 1, 1} },
            { "right",  new int[9]{2, 2, 2, 2, 2, 2, 2, 2, 2 } },
            { "left",  new int[9]{3, 3, 3, 3, 3, 3, 3, 3, 3 } },
            { "bottom",  new int[9]{4, 4, 4, 4, 4, 4, 4, 4, 4 } },
            { "back",  new int[9]{5, 5, 5, 5, 5, 5, 5, 5, 5 } },
            { "front",  new int[9]{6, 6, 6, 6, 6, 6, 6, 6, 6 } }
        };

        public enum ColorIds
        {
            White = 1,
            Blue = 2,
            Green = 3,
            Yellow = 4,
            Orange = 5,
            Red = 6,
        }

        public static Dictionary<int, Color> CubeColors = new Dictionary<int, Color>
        {
            { 1, Color.White },
            { 2, Color.FromArgb(0, 128, 255) },
            { 3, Color.FromArgb(0, 255, 0) },
            { 4, Color.FromArgb(255, 255, 0) },
            { 5, Color.FromArgb(255, 128, 64) },
            { 6, Color.FromArgb(255, 0, 0) },
        };

        private bool directionTextEnabled;
        public bool DirectionTextEnabled { get { return directionTextEnabled; } }
        private int size;
        public int Size { get { return size; } }
        private Dictionary<string, int[]> cube;
        public Dictionary<string, int[]> Cube { get { return cube; } }
        public Model3D CubeModel { get { return model; } }

        private ModelVisual3D ModelV3D;
        private Model3D model;
        private const double spacingFactor = 1.025;
        private const string relImagePath = "/View/Graphics/CubeFaces";

        private enum CubeSide
        {
            None = -1,
            Back = 0,
            Front = 1,
            Left = 2,
            Right = 3,
            Bottom = 4,
            Top = 5
        }

        public CubeGraphics(ModelVisual3D ModelV3D, Dictionary<string, int[]>? cube = null, bool directionTextEnabled = true)
        {
            this.ModelV3D = ModelV3D;
            cube ??= DefaultCube;
            UpdateModel(cube, directionTextEnabled);
        }

        /// <summary>
        /// Helper method to CreateCubelets() that adds the correct face to the meshbuilder.
        /// </summary>
        /// <param name="builder">The MeshBuilder the face should be added to.</param>
        /// <param name="position">The position of the center of the cubie</param>
        /// <param name="side">The side the face should be created at.</param>
        private void AddCubieFace(MeshBuilder builder, Point3D position, CubeSide side)
        {
            switch (side)
            {
                case CubeSide.Back:
                    builder.AddCubeFace(position, new Vector3D(-1, 0, 0), new Vector3D(0, 0, 1), 1, 1, 1);
                    break;
                case CubeSide.Front:
                    builder.AddCubeFace(position, new Vector3D(1, 0, 0), new Vector3D(0, 0, -1), 1, 1, 1);
                    break;
                case CubeSide.Left:
                    builder.AddCubeFace(position, new Vector3D(0, -1, 0), new Vector3D(0, 0, 1), 1, 1, 1);
                    break;
                case CubeSide.Right:
                    builder.AddCubeFace(position, new Vector3D(0, 1, 0), new Vector3D(0, 0, -1), 1, 1, 1);
                    break;
                case CubeSide.Bottom:
                    builder.AddCubeFace(position, new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), 1, 1, 1);
                    break;
                case CubeSide.Top:
                    builder.AddCubeFace(position, new Vector3D(0, 0, 1), new Vector3D(0, -1, 0), 1, 1, 1);
                    break;
            }
        }

        /// <summary>
        /// Creates all 3d faces for the cube. 
        /// </summary>
        /// <returns>A Model3DGroup that contains all the faces of the Cube.</returns>
        private Model3DGroup CreateCubelets()
        {
            Model3DGroup cubelets = new Model3DGroup();

            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    for (int z = 0; z < size; z++)
                    {
                        if (!(x == 0 || y == 0 || z == 0 || x == size - 1 || y == size - 1 || z == size - 1))
                            continue;

                        Model3DGroup cubelet = new Model3DGroup();
                        Point3D pos = new Point3D(x * spacingFactor, y * spacingFactor, z * spacingFactor);

                        foreach (CubeSide side in Enum.GetValues<CubeSide>())
                        {
                            if (side == CubeSide.None) continue;

                            MeshBuilder builder = new MeshBuilder(false, true);
                            AddCubieFace(builder, pos, side);

                            GeometryModel3D face = new GeometryModel3D();
                            face.Geometry = builder.ToMesh();
                            face.Material = GetFaceMaterial((x, y, z), side);

                            cubelet.Children.Add(face);
                        }
                        cubelets.Children.Add(cubelet);
                    }
                }
            }
            return cubelets;
        }

        /// <summary>
        /// Helper method to DrawSquareImage(). Rounds off the corners of a rectangle.
        /// </summary>
        /// <param name="bounds">Bounding rectangle of the rounded rectangle.</param>
        /// <param name="radius">Radius of the corners of the rounded rectangle.</param>
        /// <returns>A GraphicsPath of the rounded rectangle.</returns>
        /// <remarks>Author: György Kőszeg. Source: https://stackoverflow.com/a/33853557</remarks>
        private static GraphicsPath RoundedRect(Rectangle bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            Rectangle arc = new Rectangle(bounds.Location, size);
            GraphicsPath path = new GraphicsPath();

            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }

            // top left arc  
            path.AddArc(arc, 180, 90);

            // top right arc  
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // bottom right arc  
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // bottom left arc 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        /// <summary>
        /// Helper method to GetFaceImage(); Draws a square image.
        /// </summary>
        /// <param name="c">Square color.</param>
        /// <param name="flipType">Rotation of the image, relevant for the direction text.</param>
        /// <param name="text">Direction text.</param>
        /// <returns>A square image bitmap.</returns>
        private Bitmap DrawSquareImage(Color c, RotateFlipType flipType, string? text = null)
        {
            const int bitmapRes = 500;
            Bitmap image = new Bitmap(bitmapRes, bitmapRes);
            Graphics gr = Graphics.FromImage(image);

            gr.FillRectangle(new SolidBrush(Color.Black), 0, 0, bitmapRes, bitmapRes);
            Rectangle roundedRectRect = Rectangle.Inflate(new Rectangle(0, 0, bitmapRes, bitmapRes), (int)(bitmapRes * -0.05), (int)(bitmapRes * -0.05));
            gr.FillPath(new SolidBrush(c), RoundedRect(roundedRectRect, (int)(bitmapRes * 0.1)));

            if (text != null && directionTextEnabled)
            {
                Rectangle rect = new Rectangle(0, 0, bitmapRes, (int)(bitmapRes + bitmapRes * 0.1));

                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                gr.DrawString(text, new Font("Arial", (int)(bitmapRes * 0.5)), new SolidBrush(Color.Black), rect, stringFormat);
            }

            image.RotateFlip(flipType);

            return image;
        }

        /// <summary>
        /// Helper method to GetFaceMaterial(). Returns the side of the cube based on the cubie and one side.
        /// </summary>
        /// <param name="cubelet">A cubie.</param>
        /// <param name="side">The side of a cubie.</param>
        /// <returns>Returns which side of the cubie we are working with.</returns>
        private CubeSide GetSide((int x, int y, int z) cubelet, CubeSide side)
        {
            if (cubelet.x == 0 && side == CubeSide.Back)
                return CubeSide.Back;
            else if (cubelet.x == size - 1 && side == CubeSide.Front)
                return CubeSide.Front;
            else if (cubelet.y == 0 && side == CubeSide.Left)
                return CubeSide.Left;
            else if (cubelet.y == size - 1 && side == CubeSide.Right)
                return CubeSide.Right;
            else if (cubelet.z == 0 && side == CubeSide.Bottom)
                return CubeSide.Bottom;
            else if (cubelet.z == size - 1 && side == CubeSide.Top)
                return CubeSide.Top;
            else
                return CubeSide.None;
        }

        /// <summary>
        /// Helper method to GetFaceMaterial(). Tries to find a packed image, and creates it if it does not exist.
        /// </summary>
        /// <param name="c">Square color.</param>
        /// <param name="text">Direction text.</param>
        /// <param name="flipType">Rotation of the image, relevant for the direction text.</param>
        /// <returns>The image material.</returns>
        private Material GetFaceImage(Color c, string text, RotateFlipType flipType)
        {
            try
            {
                return MaterialHelper.CreateImageMaterial("pack://application:,,," + relImagePath + $"/{c.R} {c.G} {c.B}_{text}.bmp", 100.0);
            }
            catch (IOException)
            {
                Debug.WriteLine("!!!UNPACKED IMAGES DETECTED!!!\nSet build action of images in " + relImagePath + " to \"Resource\" or the program will not work in production! Clean solution after.");
                string relPath = "../../.." + relImagePath + $"/{c.R} {c.G} {c.B}_{text}.bmp";

                if (text == "null")
                    text = null;

                if (!File.Exists(relPath))
                {
                    if (text == "null") text = null;
                    DrawSquareImage(c, flipType, text).Save(relPath);
                }

                return MaterialHelper.CreateImageMaterial(relPath, 100.0);
            }
        }

        /// <summary>
        /// Gets the material of a face, for example a red square.
        /// </summary>
        /// <param name="cubelet">The coordinates of a cubie</param>
        /// <param name="side">The side of a cubie</param>
        /// <returns>The material for the face</returns>
        private Material GetFaceMaterial((int x, int y, int z) cubelet, CubeSide side)
        {
            int maxIndex = size * size - 1;
            Color c;
            string? text = null;
            RotateFlipType flipType;

            CubeSide cubeSide = GetSide(cubelet, side);

            switch (cubeSide)
            {
                case CubeSide.Back:
                    flipType = RotateFlipType.RotateNoneFlipNone;

                    c = CubeColors[cube["back"][maxIndex - (size * cubelet.z + cubelet.y)]];

                    if (cubelet.y == 1 && cubelet.z == 1)
                        text = "B";
                    break;
                case CubeSide.Front:
                    flipType = RotateFlipType.Rotate180FlipNone;

                    c = CubeColors[cube["front"][size * (2 - cubelet.z) + cubelet.y]];

                    if (cubelet.y == 1 && cubelet.z == 1)
                        text = "F";
                    break;
                case CubeSide.Left:
                    flipType = RotateFlipType.RotateNoneFlipNone;

                    c = CubeColors[cube["left"][maxIndex - (size * cubelet.z + (2 - cubelet.x))]];

                    if (cubelet.x == 1 && cubelet.z == 1)
                        text = "L";
                    break;
                case CubeSide.Right:
                    flipType = RotateFlipType.Rotate180FlipNone;

                    c = CubeColors[cube["right"][maxIndex - (size * cubelet.z + cubelet.x)]];

                    if (cubelet.x == 1 && cubelet.z == 1)
                        text = "R";
                    break;
                case CubeSide.Bottom:
                    flipType = RotateFlipType.Rotate270FlipNone;

                    c = CubeColors[cube["bottom"][maxIndex - (size * cubelet.x + (2 - cubelet.y))]];

                    if (cubelet.y == 1 && cubelet.x == 1)
                        text = "D";
                    break;
                case CubeSide.Top:
                    flipType = RotateFlipType.Rotate90FlipNone;

                    c = CubeColors[cube["top"][size * cubelet.x + cubelet.y]];

                    if (cubelet.y == 1 && cubelet.x == 1)
                        text = "U";
                    break;
                default:
                    return MaterialHelper.CreateMaterial(Colors.Black);
            }

            if (directionTextEnabled)
                text ??= "null";
            else
                text = "null";

            return GetFaceImage(c, text, flipType);
        }

        /// <summary>
        /// Generates a new model, which is among others necessary for updating the squares.
        /// </summary>
        /// <param name="cube">A dictionary containing the colors of the squares</param>
        /// <param name="directionTextEnabled">Determines whether directions are on the cube or not.</param>
        /// <exception cref="Exception">Cube dictionary does not match size.</exception>
        public void UpdateModel(Dictionary<string, int[]> cube, bool directionTextEnabled = true)
        {
            this.directionTextEnabled = directionTextEnabled;
            this.cube = cube;
            double size = Math.Sqrt(this.cube["front"].Length);
            if (size % 1 == 0)
                this.size = (int)size;
            else
                throw new Exception("Invalid cube!");

            Model3DGroup cubelets = CreateCubelets();

            MeshBuilder builder = new MeshBuilder(false, true);
            builder.AddSphere(new Point3D(1 * spacingFactor, 1 * spacingFactor, 1 * spacingFactor), 0.475 * this.size * spacingFactor);
            cubelets.Children.Add(new GeometryModel3D { Geometry = builder.ToMesh(), Material = MaterialHelper.CreateMaterial(Colors.Black) });

            model = cubelets;
        }

        /// <summary>
        /// Draws the cube to the screen.
        /// </summary>
        public void DrawCube()
        {
            ModelV3D.Content = model;
        }
    }
}
