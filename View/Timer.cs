﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows;
using System.Diagnostics;

namespace rubikscube
{
    internal class Timer
    {
        public bool StartOnMove;
        public bool CheatMode;
        public bool Enabled
        {
            get { return t != null && t.IsEnabled; }
        }
        public DateTime? TimerStart;

        public Timer(MainWindow window)
        {
            Window = window;
            TimerTextBlock = (TextBlock)window.FindName("timer");
            TimerButton = (Button)window.FindName("timer_button");
        }

        private MainWindow Window;
        private TextBlock TimerTextBlock;
        private Button TimerButton;

        private DispatcherTimer? t;

        /// <summary>
        /// Helper method to Start(). Sets the actual Timer.
        /// </summary>
        private void setTimer()
        {
            TimerStart = DateTime.Now;

            void updateTimer(object? sender, EventArgs e)
            {
                TimeSpan elapsedTime = DateTime.Now.Subtract((DateTime)TimerStart);

                string format;

                if (elapsedTime.TotalMinutes <= 59)
                    format = @"mm\:ss";
                else if (elapsedTime.TotalHours < 24)
                    format = @"hh\:mm\:ss";
                else
                    format = @"d\:hh\:mm\:ss";

                TimerTextBlock.Text = DateTime.Now.Subtract((DateTime)TimerStart).ToString(format);
            }

            t = new DispatcherTimer(DispatcherPriority.Send);
            t.Tick += new EventHandler(updateTimer);
            t.Interval = new TimeSpan(0, 0, 0, 0, 200);
            t.Start();
        }

        /// <summary>
        /// Starts the manual solve timer.
        /// </summary>
        /// <returns>If the timer started or not.</returns>
        public bool Start()
        {
            if (CubeOperations.CheckIfSolved(Window.thisCube))
            {
                MessageBox.Show("You cannot solve a cube that is already solved.\nTry scrambling or importing a cube.", "That was easy!", MessageBoxButton.OK, MessageBoxImage.Stop);
                return false;
            }

            CheckBox startOnMoveCheckBox = (CheckBox)Window.FindName("startonmove_checkbox");

            startOnMoveCheckBox.IsChecked = false;
            Window.ChangeStartOnMove(startOnMoveCheckBox, null);
            TimerButton.IsEnabled = true;

            setTimer();
            RefreshUI();

            TimerTextBlock.Text = "00:00";
            TimerTextBlock.Visibility = Visibility.Visible;
            TimerButton.Content = "Reset";

            Window.SetResetCube();
            Window.thisCube.RedoStack.Clear();

            return true;
        }

        /// <summary>
        /// Stops the manual solve timer.
        /// </summary>
        /// <returns>if the timer stopped or not.</returns>
        public bool Stop()
        {
            if (!Enabled)
                return false;

            t.Stop();
            TimerStart = null;

            RefreshUI();

            if (CheatMode)
                ToggleCheatMode((Button)Window.FindName("enablecheatmode_button"));

            TimerTextBlock.Visibility = Visibility.Collapsed;
            TimerButton.Content = "Start";

            return true;
        }

        /// <summary>
        /// Ends the timer when the cube was solved, showing the user the final time and total moves.
        /// </summary>
        /// <exception cref="NullReferenceException">Cube cannot be solved if the timer was not running.</exception>
        public void CubeSolvedManually()
        {
            if (!Enabled || TimerStart == null) throw new NullReferenceException("Cube cannot be solved if the timer was not running.");
            TimeSpan elapsed = DateTime.Now.Subtract((DateTime)TimerStart);

            Stop();

            MessageBox.Show($"You solved the cube in {Window.thisCube.PermsTextList.Count - Window.resetCube.PermsTextList.Count} moves.                           " +
                "\n\nYour final time is:\n" + elapsed.ToString(), "Congratulations!");
        }

        /// <summary>
        /// Enables cheat mode, which shows the user the moves they made while solving the cube, and enables undo/redo.
        /// </summary>
        /// <param name="b">The button that enables cheat mode.</param>
        public void ToggleCheatMode(Button b)
        {
            b.Visibility = CheatMode ? Visibility.Visible : Visibility.Collapsed;
            ((ScrollViewer)Window.FindName("steps_done")).Visibility = CheatMode ? Visibility.Collapsed : Visibility.Visible;

            if (!CheatMode)
                Window.UpdatePerms(Window.resetCube.PermsTextList.Count);

            CheatMode = !CheatMode;
            RefreshUI();
        }

        /// <summary>
        /// Refreshes the properties used as XAML bindings.
        /// </summary>
        private void RefreshUI()
        {
            Window.OnPropertyChanged("TimerEnabled");
            Window.OnPropertyChanged("TimerDisabled");
            Window.OnPropertyChanged("UndoRedoEnabled");
        }
    }
}
