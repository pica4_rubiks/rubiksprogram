# RubiksProgram - Import, Scramble, Solve!

This C# application written using [WPF](https://learn.microsoft.com/en-us/dotnet/desktop/wpf/overview/?view=netdesktop-8.0) and the [Helix toolkit](https://helix-toolkit.github.io/) to use a virtual 3x3x3 Rubik's cube in 3D. With this application, one is able to:
- import a real-world cube, 
- perform rotations on the cube
- generate a random scrambled cube, 
- and solve scrambled rubiks cube using the CFOP method.

## Table of contents

[TOC]

## Disclaimer

This program only supports the official Rubik's Cube color scheme. Any third-party cubes with a different scheme will have to be converted.

## Installation
To run this application, one can download the sulution as a .zip file by clicking on the <kbd>code</kbd> button, and then selecting .zip.
Unzip the folder, and open rubikscube.sln in your editor of choice (this manual will explain Visual Studio).

### Compiling 
<em> If you use a different IDE than visual studio, please refer to the manual of your IDE.</em>

- In the top menubar, select <kbd>Build</kbd>.
- Next, select <kbd>Build Solution</kbd> from the drop-down.
- Navigate to the directory where you just opened the rubikscube.sln file. then, navigate to "bin\Release\net7.0-windows".
- Open rubikscube.exe. 


## Usage 

When opening the project, you are greeted with the following screen, consisting of a cube, and a control menu.

<img src="Files/ReadMeImages/startscreen.png" height="300"> startscreen

### Manual- and solver mode

There are 2 different modes; the "automatic" mode, and the "manual" mode. The automatic mode allows you to let the program generate a solution for the cube, and walk through that solution step-by-step. The manual mode allows you to time yourself solving the cube, from a scrambled state.

To switch from "automatic" to "manual" and back, simply press the button highlighted in the image below:

<img src="Files/ReadMeImages/modes.png" height="300"> switching from automatic to manual and back

### Reset cube button, and Face notation

On the left side of the program, there is the left hand menu. At the top of this menu, there are the "face notation" checkbox, and the <kbd>Reset cube</kbd> button.

<img src="Files/ReadMeImages/resetAndFacePlace.png" height="300"> location of face notation and reset button

#### Face notation
The face notation either enables or disables the black letters on the 3d cube. These can be used to indicate which face is which, and can be en- or disabled using this checkbox.

#### Reset Cube
The reset cube resets the cube to a checkpoint. Checkpoints are set when you scramble or import a cube. If you reset to a checkpoint, the <kbd>Reset cube</kbd> button will change to <kbd>Complete reset</kbd>.  Clicking it will now reset the cube to a fully solved state.

### Viewport Controls
The viewport is the "camera" of the program. You are able to manipulate the viewport in 2 ways; rotation and zoom.

#### Viewport rotation
To rotate the cube, you can either:
- Use the arrow keys (<kbd>&uarr;</kbd>, <kbd>&darr;</kbd>, <kbd>&larr;</kbd>, <kbd>&rarr;</kbd>), or
- Hold <kbd>Ctrl</kbd> and press one of the face keys (<kbd>F</kbd>, <kbd>B</kbd>, <kbd>L</kbd>, <kbd>R</kbd>, <kbd>U</kbd>, <kbd>D</kbd>), or
- press one of the faces on the small cube in the bottom right corner (see image below)
- Hold <kbd>RightMouseClick</kbd> or <kbd>LeftMouseClick</kbd> and drag your cursor. The logo as in the image below will indicate you can drag.

<img src="Files/ReadMeImages/mouseclick_drag.png" height="100"> drag logo


<img src="Files/ReadMeImages/smallCube.png" height="100"> small cube in bottom right corner


#### Viewport Zoom

Zooming in- and out of the cube can be done the following ways:
- by pinching or spreading 2 fingers on your trackpad
- by rotating the scroll wheel on your mouse


### Cube rotating

Rotating the cube can be done in 2 ways; by using the lefthand menu, or the keyboard shortcuts.

#### Lefthand menu
The left hand menu has 2 parts. The amount part has 3 radio buttons, where you can choose between 1 rotation (in the case of F that would be F), 2 rotations (F2), or 3 rotations (F'). After selecting the amount in the amount part, a button (<kbd>Upward</kbd>, <kbd>Left</kbd>, <kbd>Front</kbd>, <kbd>Right</kbd>, <kbd>Downward</kbd>, <kbd>Back</kbd>) in the rotation part can be pressed to rotate the cube [amount] times on the [rotation] face.

<img src="Files/ReadMeImages/rotations_LeftHandMenu.png" height="300"> rotating the cube using the left-hand menu

#### Keyboard shortcuts

The cube can also be rotated using the following keyboard shortcuts:
- <kbd>F</kbd> does one front rotation
- <kbd>B</kbd>: does one back rotation
- <kbd>L</kbd>: does one left rotation
- <kbd>R</kbd>: does one right rotation
- <kbd>U</kbd>: does one up rotation
- <kbd>D</kbd>: does one down rotation
- <kbd>M</kbd>: does one rotation of the middle layer over the X axis
- <kbd>E</kbd>: does one rotation of the middle layer over the X axis
- <kbd>S</kbd>: does one rotation of the middle layer over the X axis
- <kbd>X</kbd>: does one rotation of the full cube over the X axis
- <kbd>Y</kbd>: does one rotation of the full cube over the Y axis
- <kbd>Z</kbd>: does one rotation of the full cube over the Z axis
- optionally, when holding down <kbd>Shift</kbd> and pressing one of the buttons stated above, a counterclockwise rotation will be done instead of a clockwise rotation.

#### Undo and Redo

You can undo and redo moves using the buttons under the "Action" menu or by using <kbd>Ctrl+Z</kbd> or <kbd>Ctrl+Shift+Z</kbd> respectively.

### Seed-based scramble generator

In order to generate the same scrambled cube over and over again, you are able to enter a seed. When entering the same seed, the same scramble will be generated.
The seed menu consists of 3 parts:
- Seed input field (the textbox after "seed:"): here one can a seed.
- Random seed button: this button generates a (pseudo-)random seed, and enters that seed into the seed input field.
- Scramble button: scrambles the cube based on the seed in the seed input field.

<img src="Files/ReadMeImages/seedScramble.png" height="300"> seed menu



### Importing- and exporting cube files

In this program, you are able to import- and export the state of a cube. 

#### Import

To import a cube: 
- Press <kbd>File</kbd>, then <kbd>Open</kbd>. 
- Then, navigate to the desired file, and press <kbd>Open</kbd>

#### Export

To export a cube:
- Press <kbd>File</kbd>, then <kbd>Save</kbd>.
- Change the name of the file to the desired name, and make sure filetype JSON is selected (location 1 in image).
- Navigate to the directory where you want to save the cube file (location 2 in image).
- Press <kbd>Save</kbd> (location 3 in image).

<img src="Files/ReadMeImages/howToSave.png" height="300"> how to save a cube


### Manually import cube you have in real life

In order to manually import a cube, click on <kbd>Cube</kbd> in the top-left corner. 

<img src="Files/ReadMeImages/ManualCubeImortLocation.png" height="300"> location of manual import button


Then, select <kbd>Manual Import</kbd>. You should now see the following screen:

<img src="Files/ReadMeImages/ManualCubeImport.png" height="300"> manual import screen


Here, you are able to import a cube based on a cube in real life. When you have colored in your cube, click <kbd>Import</kbd>. 

There might be some text stating your cube is not valid, and the reason why. In that case, please double-check your virtual cube against the cube you are holding in your hand and make sure you did not make any mistakes.

### Using the solve feature

 When in the solver mode, in the bottom of the left-hand control menu, there is a text field called "Solution:". When performing the perms in that field, The cube will be solved. Alternatively, press the <kbd>play</kbd>/<kbd>pause</kbd> button to automatically solve the cube step-by-step. If you have missed a step, you can use the <kbd>Previous step</kbd> and <kbd>Next step</kbd> buttons to go back- or forwards in the solution. 

It might be the case that you have a long solution, or the solution might not be available after importing a cube.

In that case, press the <kbd>Solve Cube</kbd>/<kbd>Recalculate solution</kbd> button, which calculates a new solution to the cube.

